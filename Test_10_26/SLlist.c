#define _CRT_SECURE_NO_WARNINGS 1

#include "SLlist.h"

void SeqListInit(SeqList* ps)
{
	assert(ps);
	ps->a = (SLDateType*)malloc(sizeof(SLDateType) * 4);
	if (ps->a == NULL)
	{
		printf("����ʧ��!\n");
		return;
	}
	ps->capacity = 4;
	ps->size = 0;
	memset(ps->a, 0, sizeof(SLDateType) * 4);
}

void SeqListDestroy(SeqList* ps)
{
	assert(ps->a);
	free(ps->a);
	ps->capacity = 0;
	ps->size = 0;
}

void SeqListPrint(SeqList* ps)
{
	int i = 0;
	if (ps->size <= 0)
	{
		printf("û�����ݣ�\n");
		return;
	}
	for (i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

void SeqListPushBack(SeqList* ps, SLDateType x)
{
	assert(ps);
	if (ps->capacity == ps->size)
	{
		Add_capacity(ps);
	}
	ps->a[ps->size++] = x;
}

void Add_capacity(SeqList* ps)
{
	SLDateType* p = (SLDateType*)realloc(ps->a, sizeof(SLDateType) * (ps->capacity + 4));
	if (p == NULL)
	{
		printf("����ʧ�ܣ�\n");
		exit(-1);
	}
	ps->a = p;
	ps->capacity += 4;
}

void SeqListPushFront(SeqList* ps, SLDateType x)
{
	assert(ps);
	if (ps->capacity == ps->size)
	{
		Add_capacity(ps);
	}
	for (int i = ps->size - 1; i >= 0; i--)
	{
		ps->a[i + 1] = ps->a[i];
	}
	ps->a[0] = x;
	ps->size++;
}

void SeqListPopFront(SeqList* ps)
{
	assert(ps->size > 0);
	for (int i = 0; i < ps->size - 1; i++)
	{
		ps->a[i] = ps->a[i + 1];
	}
	ps->size--;
}

void SeqListPopBack(SeqList* ps)
{
	assert(ps->size > 0);
	ps->size--;
}


int SeqListFind(SeqList* ps, SLDateType x)
{
	assert(ps->size > 0);
	for (int i = 0; i < ps->size; i++)
	{
		if (ps->a[i] == x)
		{
			return i;
		}
	}
	return -1;
}

void SeqListInsert(SeqList* ps, size_t pos, SLDateType x)
{
	assert(ps->size >= pos);
	if (ps->capacity == ps->size)
	{
		Add_capacity(ps);
	}
	for (int i = 0; i < ps->size; i++)
	{
		if (i == pos - 1)
		{
			for (int j = ps->size - 1; j >= i; j--)
			{
				ps->a[j + 1] = ps->a[j];
			}
			ps->a[i] = x;
			ps->size++;
			break;
		}
	}
}

void SeqListErase(SeqList* ps, size_t pos)
{
	assert(ps->size >= pos);
	for (int i = 0; i < ps->size; i++)
	{
		if (i == pos - 1)
		{
			for (int j = i; j < ps->size; j++)
			{
				ps->a[j] = ps->a[j + 1];
			}
			ps->size--;
			break;
		}
	}
}
