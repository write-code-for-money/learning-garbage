#define _CRT_SECURE_NO_WARNINGS 1

#include "SLlist.h"

void test()
{
	SeqList SL;
	SeqListInit(&SL);
	//SeqListDestroy(&SL);
	//SeqListPrint(&SL);
	SeqListPushBack(&SL, 4);
	SeqListPushBack(&SL, 5);
	SeqListPushFront(&SL, 1);
	SeqListPushFront(&SL, 2);
	SeqListPushFront(&SL, 3);
	//SeqListPopFront(&SL);
	//SeqListPopFront(&SL);
	//SeqListPopFront(&SL);
	//SeqListPopFront(&SL);
	//SeqListPopBack(&SL);
	SeqListInsert(&SL, 1, 9);
	SeqListInsert(&SL, 2, 10);
	SeqListPrint(&SL);

	SeqListErase(&SL, 2);


	/*int x = SeqListFind(&SL, 4);
	printf("%d\n", SL.a[x]);*/

	SeqListPrint(&SL);
	SeqListDestroy(&SL);
}



int main()
{
	test();
	return 0;
}