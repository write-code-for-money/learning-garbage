#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	int n = 9;
//	float* pFloat = (float*)&n;
//	printf("n的值为：%d\n", n);
//	printf("*pFloat的值为：%f\n", *pFloat);
//	*pFloat = 9.0;
//	printf("num的值为：%d\n", n);
//	printf("*pFloat的值为：%f\n", *pFloat);
//	return 0;
//}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
//int* findErrorNums(int* nums, int numsSize, int* returnSize) {
//    static int arr[2] = { 0, 0 };
//    int i = 0;
//    int j = 0;
//    int t = 0;
//    int flag = 0;
//    for (i = 1; i <= numsSize; i++)
//    {
//        for (j = 0; j < numsSize; j++)
//        {
//            if (nums[j] == i)
//            {
//                break;
//            }
//        }
//        if (j == numsSize)
//        {
//            arr[1] = i;
//            break;
//        }
//    }
//    for (i = 0; i < numsSize; i++)
//    {
//        t = nums[i];
//        for (j = 0; j < numsSize; j++)
//        {
//            if ((j != i) && (nums[j] == t))
//            {
//                arr[0] = t;
//                flag = 1;
//                break;
//            }
//        }
//        if (flag == 1)
//        {
//            break;
//        }
//    }
//    *returnSize = 2;
//    return arr;
//}

#include <stdio.h>
#include <string.h>

int main()
{
    int n = 0;
    char arr[101] = { 0 };
    int sum = 0;
    int sum2 = 0;
    int sum3 = 0;
    int other = 0;
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        scanf("%s", arr);
        int len = strlen(arr);
        if (len < 8)
        {
            printf("NO\n");
            continue;
        }
        if (arr[0] >= '0' && arr[0] <= '9')
        {
            printf("NO\n");
            continue;
        }
        for (int j = 0; j < len; j++)
        {
            if (arr[j] >= 'a' && arr[j] <= 'z')
            {
                sum++;
            }
            else if (arr[j] >= '0' && arr[j] <= '9')
            {
                sum2++;
            }
            else if (arr[j] >= 'A' && arr[j] <= 'Z')
            {
                sum3++;
            }
            else {
                other++;
            }
        }
        if (other > 0)
        {
            printf("NO\n");
            continue;
        }
        if ((sum > 0) + (sum2 > 0) + (sum3 > 0) < 2)
        {
            printf("NO\n");
            continue;
        }
        printf("YES\n");
    }
    return 0;
}
