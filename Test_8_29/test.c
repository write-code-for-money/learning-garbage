#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int sum = 0;
//    int x = 1;
//    int y = 1;
//    if (n <= 2 && n > 0)
//    {
//        printf("%d\n", 1);
//    }
//    while (n > 2)
//    {
//        sum = x + y;
//        x = y;
//        y = sum;
//        n--;
//    }
//
//    printf("%d\n", sum);
//    return 0;
//}

#include <stdio.h>
#include <math.h>

int main()
{
    double n = 0.00;
    int m = 0;
    while (scanf("%lf %d", &n, &m) == 2)
    {
        double sum = 0.00;
        for (int i = 0; i < m; i++)
        {
            sum += n;
            n = sqrt(n);
        }
        printf("%.2lf\n", sum);
    }
    return 0;
}