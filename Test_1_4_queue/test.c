#define _CRT_SECURE_NO_WARNINGS 1

#include "Queue.h"


int main()
{
	Queue q;
	// 初始化队列
	QueueInit(&q);
	// 队尾入队列
	QueuePush(&q, 1);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	QueuePush(&q, 4);
	print(&q);
	// 队头出队列
	QueuePop(&q);
	print(&q);
	// 获取队列头部元素
	printf("%d\n", QueueFront(&q));
	// 获取队列队尾元素
	printf("%d\n", QueueBack(&q));
	// 获取队列中有效元素个数
	printf("%d\n", QueueSize(&q));
	// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
	printf("%d\n", QueueEmpty(&q));
	// 销毁队列
	QueueDestroy(&q);
	print(&q);
	return 0;
}