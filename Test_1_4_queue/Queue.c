#define _CRT_SECURE_NO_WARNINGS 1

#include "Queue.h"

// 初始化队列
void QueueInit(Queue* q)
{
	assert(q);
	q->_front = q->_rear = NULL;
}

// 队尾入队列
void QueuePush(Queue* q, QDataType data)
{
	QNode* node = (QNode*)malloc(sizeof(QNode));
	node->_data = data;
	node->_pNext = NULL;
	if (q->_rear == NULL)
	{
		q->_front = q->_rear = node;
	}
	else
	{
		q->_rear->_pNext = node;
		q->_rear = node;
	}
}


void print(Queue* q)
{
	QNode* p = q->_front;
	while (p != NULL)
	{
		printf("%d->", p->_data);
		p = p->_pNext;
	}
	printf("NULL\n");
}

// 队头出队列
void QueuePop(Queue* q)
{
	if (q->_front == NULL)
	{
		printf("nothing to pop\n");
		return;
	}
	QNode* node = q->_front;
	q->_front = q->_front->_pNext;
	node->_pNext = NULL;
	free(node);
}

// 获取队列头部元素
QDataType QueueFront(Queue* q)
{
	return q->_front->_data;
}

// 获取队列队尾元素
QDataType QueueBack(Queue* q)
{
	return q->_rear->_data;
}

// 获取队列中有效元素个数
int QueueSize(Queue* q)
{
	int sz = 0;
	QNode* p = q->_front;
	while (p != NULL)
	{
		sz++;
		p = p->_pNext;
	}
	return sz;
}

// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
int QueueEmpty(Queue* q)
{
	return q->_rear == NULL ? 1 : 0;
}

// 销毁队列
void QueueDestroy(Queue* q)
{
	q->_rear = NULL;
	while (q->_front)
	{
		QNode* p = q->_front;
		q->_front = p->_pNext;
		p->_pNext = NULL;
		free(p);
	}
}
