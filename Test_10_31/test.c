#define _CRT_SECURE_NO_WARNINGS 1

#include "SList.h"

void test()
{
	printf("create\n");
	SListNode* phead = NULL;
	phead = BuySListNode(2);
	SListNode* pend = phead;
	pend->next = BuySListNode(3);
	pend = pend->next;
	pend->next = BuySListNode(4);
	pend = pend->next;
	SListPrint(phead);

	printf("PushBack\n");
	SListPushBack(&phead, 9);
	SListPushBack(&phead, 8);
	SListPrint(phead);

	printf("PushFront\n");
	SListPushFront(&phead, 5);
	SListPushFront(&phead, 11);
	SListPushFront(&phead, 15);
	SListPrint(phead);

	printf("PopBack\n");
	SListPopBack(&phead);
	SListPopBack(&phead);
	SListPopBack(&phead);
	SListPopBack(&phead);
	//SListPrint(phead);
	//SListPopBack(&phead);
	//SListPopBack(&phead);
	//SListPopBack(&phead);
	//SListPopBack(&phead);
	SListPrint(phead);

	printf("popfront\n");
	SListPopFront(&phead);
	SListPopFront(&phead);
	/*SListPopFront(&phead);
	SListPopFront(&phead);
	SListPopFront(&phead);*/
	SListPrint(phead);

	printf("find\n");
	SListPrint(SListFind(phead, 5));

	printf("SListInsertAfter\n");
	SListInsertAfter(phead, 10);
	SListInsertAfter(SListFind(phead, 10), 11);
	SListPrint(phead);

	printf("SListEraseAfter\n");
	//SListEraseAfter(phead);
	SListEraseAfter(SListFind(phead, 10));
	//SListEraseAfter(phead);
	//SListEraseAfter(phead);
	SListPrint(phead);

	SListDestroy(phead);
	phead = NULL;
	SListPrint(phead);

}

int main()
{
	test();

	return 0;
}