#define _CRT_SECURE_NO_WARNINGS 1


//#include <stdio.h>
//
//int compare(int a, int b, int c)
//{
//    int max = a;
//    if (max < b)
//    {
//        max = b;
//    }
//    if (max < c)
//    {
//        max = c;
//    }
//    return max;
//}
//
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    while (scanf("%d %d %d", &a, &b, &c) == 3)
//    {
//        int max = compare(a, b, c);
//        if (max == a && b + c > a && a - c < b)
//        {
//            if (a == b && b == c)
//            {
//                printf("Equilateral triangle!\n");
//            }
//            else if (a == b || a == c || b == c)
//            {
//                printf("Isosceles triangle!\n");
//            }
//            else {
//                printf("Ordinary triangle!\n");
//            }
//        }
//        else if (max == b && a + c > b && b - c < a)
//        {
//            if (a == b && b == c)
//            {
//                printf("Equilateral triangle!\n");
//            }
//            else if (a == b || a == c || b == c)
//            {
//                printf("Isosceles triangle!\n");
//            }
//            else {
//                printf("Ordinary triangle!\n");
//            }
//        }
//        else if (max == c && a + b > c && c - b < a)
//        {
//            if (a == b && b == c)
//            {
//                printf("Equilateral triangle!\n");
//            }
//            else if (a == b || a == c || b == c)
//            {
//                printf("Isosceles triangle!\n");
//            }
//            else {
//                printf("Ordinary triangle!\n");
//            }
//        }
//        else {
//            printf("Not a triangle!\n");
//        }
//    }
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    char arr = 0;
//    int sum_a = 0;
//    int sum_b = 0;
//    while (scanf("%c", &arr) == 1 && arr != '0')
//    {
//        if (arr == 'A')
//        {
//            sum_a++;
//        }
//        else {
//            sum_b++;
//        }
//    }
//    if (sum_a > sum_b)
//    {
//        printf("A\n");
//    }
//    else if (sum_b > sum_a)
//    {
//        printf("B\n");
//    }
//    else {
//        printf("E\n");
//    }
//    return 0;
//}

//#include <stdio.h>
//
//int compare(void* e1, void* e2)
//{
//    return *(int*)e1 - *(int*)e2;
//}
//
//
//int main()
//{
//    int arr[7] = { 0 };
//    while (scanf("%d %d %d %d %d %d %d", &arr[0], &arr[1], &arr[2], &arr[3], &arr[4], &arr[5],
//        &arr[6]) == 7)
//    {
//        qsort(arr, 7, 4, compare);
//        int sum = 0;
//        for (int i = 1; i < 6; i++)
//        {
//            sum += arr[i];
//        }
//        printf("%.2f\n", sum / 5.0);
//    }
//
//    return 0;
//}

#include <stdio.h>

int main()
{
    int num = 0;
    while (scanf("%d", &num) == 1)
    {
        switch (num)
        {
        case 200:
        {
            printf("OK\n");
            break;
        }
        case 202:
        {
            printf("Accepted\n");
            break;
        }
        case 400:
        {
            printf("Bad Request\n");
            break;
        }
        case 403:
        {
            printf("Forbidden\n");
            break;
        }
        case 404:
        {
            printf("Not Found\n");
            break;
        }
        case 500:
        {
            printf("Internal Server Error\n");
            break;
        }
        case 502:
        {
            printf("Bad Gateway\n");
            break;
        }
        default:
        {
            break;
        }
        }
    }
    return 0;
}