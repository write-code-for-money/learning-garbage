#define _CRT_SECURE_NO_WARNINGS 1

//int findMaxConsecutiveOnes(int* nums, int numsSize) {
//    int i = 0;
//    int sum = 0;
//    int count = 0;
//    for (i = 0; i < numsSize; i++)
//    {
//        if (nums[i] == 1)
//        {
//            sum++;
//        }
//        if (nums[i] == 0)
//        {
//            if (sum > count)
//            {
//                count = sum;
//            }
//            sum = 0;
//        }
//    }
//    if (sum > count)
//    {
//        count = sum;
//    }
//    return count;
//}

#include <stdio.h>

int main()
{
    int n = 0;
    scanf("%d", &n);
    int i = 0;
    int j = 0;
    int sum = 0;
    int count = 0;
    for (i = 1; i <= n; i++)
    {
        sum = 0;
        for (j = 1; j <= i; j++)
        {
            if (i % j == 0 && i != j)
            {
                sum += j;
            }
        }
        if (sum == i)
        {
            count++;
        }
    }
    printf("%d\n", count);
    return 0;
}