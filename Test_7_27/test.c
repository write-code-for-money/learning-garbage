#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//汉诺塔问题
//有A，B，C三个柱
//将A柱上的n个盘子移动到C柱上，始终保持下面的小
//思路：
//将A柱上的n-1个盘子移动到B柱
//将剩下的一个盘子移动到C柱
//将B柱的n-1个盘子移动到C柱
//

void move(char x, char y)
{
	printf("%c-->%c\n", x, y);
}

void hanoi(int n, char x, char y, char z)
{
	/*char x = 'A';
	char y = 'B';
	char z = 'C';*/
	if (n == 1)
	{
		move(x, z);
		//printf("%c-->%c\n", x, z);
	}
	else
	{
		hanoi(n - 1, x, z, y);
		//printf("%c-->%c\n", x, z);
		move(x, z);
		hanoi(n - 1, y, x, z);
	}
}

//void move(char x, char y)
//{
//	printf("%c-->%c\n", x, y);
//}



int main(void)
{
	
	int n = 0;
	scanf("%d", &n);  //输入n个盘子
	hanoi(n, 'A', 'B', 'C');
	//printf("%d\n", sum);
	return 0;
}