#define _CRT_SECURE_NO_WARNINGS 1

//#define Jan 1
//#define Feb 2
//#define Mar 3
//#define Apr 4
//#define May 5
//#define Jun 6
//#define Jul 7
//#define Aug 8
//#define sep 9
//#define Oct 10
//#define Nov 11
//#define Dec 12

#include <stdio.h>

//enum year
//{
//	Jan = 1, Feb, Mar, Apr, May,
//	Jun, Jul, Aug, Sep, Oct,
//	Nov, Dec
//};
//enum year year_day;   //枚举定义

//enum year
//{
//	Jan = 1, Feb, Mar, Apr, May,
//	Jun, Jul, Aug, Sep, Oct,
//	Nov, Dec
//}year_day;

//enum
//{
//	Jan = 1, Feb, Mar, Apr, May,
//	Jun, Jul, Aug, Sep, Oct,
//	Nov, Dec
//}year_day;

//enum year
//{
//	Jan, Feb = 3, Mar, Apr, May,
//	Jun, Jul, Aug, Sep, Oct,
//	Nov, Dec
//};


//int main(void)
//{
//	//enum year year_day;   //枚举定义
//	for (year_day = Jan; year_day <= Dec; year_day++)
//	{
//		printf("%d ", year_day);
//	}
//	printf("\n");
//	return 0;
//}

//int sub(int n, int k)
//{
//	if (k == 0)
//	{
//		return 1;
//	}
//	else
//	{
//		return n * sub(n, k - 1);
//	}
//}
//
//int main(void)
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &n, &k);
//	int sum = sub(n, k);
//	printf("%d\n", sum);
//	return 0;
//}

//int DigitSum(int n)
//{
//	if (n > 9)
//	{
//		return (n % 10) + DigitSum(n / 10);
//	}
//	else
//	{
//		return n;
//	}
//}
//
//int main(void)
//{
//	int n = 0;
//	scanf("%d", &n);
//	int num = DigitSum(n);
//	printf("%d\n", num);
//	return 0;
//}

void reverse_string(char* arr)
{
	int i = 0;
	while (arr[i] != '\0')
	{
		i++;
	}

	char ch = *arr;
	*arr = *(arr+i-1);
	*(arr + i - 1) = '\0';
	if (i - 2 > 0)
	{
		reverse_string(arr + 1);
	}
	*(arr + i - 1) = ch;
}

int main(void)
{
	char arr[10] = {"0"};
	scanf("%s", arr);
	printf("开始：%s\n", arr);
	reverse_string(arr);
	printf("结束：%s\n", arr);
	return 0;
}

//#include <stdio.h>
//#include <stdlib.h>
//
//int my_strLen(char str[]);//函数声明
//void reverse_string(char str[]);//函数声明
//
//
//int my_strLen(char str[]) {//需要一个求数组长度的函数 自定义一个my_strLen
//	if (str[0] == '\0') {//当数组开头为结束标志符\0时 表示数组已经遍历完毕
//		return 0;
//	}
//	return 1 + my_strLen(str + 1);//数组长度等于1 + 以第二个元素开头的数组长度
//}
//
////逆序函数
//void reverse_string(char str[]) {
//	int len = my_strLen(str);//首先求出数组长度 长度-1即为数组内最后一个字符下标
//	char tem = *str;//定义一个临时变量储存首字符的内容
//	*str = *(str + len - 1);//将最后一个元素赋值给第一个字符，完成第一组逆序
//	*(str + len - 1) = '\0';//将\0赋值给最后一个字符，使递归找到最后一个字符
//	if (my_strLen(str) > 0) {//如果数组长度不小于0 则一直递归下去
//		reverse_string(str + 1);
//	}
//	*(str + len - 1) = tem;//数组长度小于0，即后半部分已经逆序完毕
//}						//此时将前半部分的值逐个速出即可，就是tem里面存储的值
//
//int main() {
//	char str[10] = "0";
//	scanf("%s", str);
//	printf("before :%s\n", str);
//	reverse_string(str);
//	printf("after :%s\n", str);
//
//	system("pause");
//	return 0;
//}