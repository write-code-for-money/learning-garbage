#define _CRT_SECURE_NO_WARNINGS 1

#include "queue.h"

// 初始化队列 
void QueueInit(Queue* q)
{
	assert(q);
	q->_front = NULL;
	q->_rear = NULL;
}

QNode* BuyNode(QDataType data)
{
	QNode* node = (QNode*)malloc(sizeof(QNode));
	node->_data = data;
	node->_next = NULL;
	return node;
}

// 队尾入队列 
void QueuePush(Queue* q, QDataType data)
{
	assert(q);
	QNode* new = BuyNode(data);
	if (q->_front == NULL)
	{
		q->_front = new;
		q->_rear = new;
	}
	else
	{
		q->_rear->_next = new;
		q->_rear = new;
	}
}

// 队头出队列 
void QueuePop(Queue* q)
{
	assert(q);
	if (q->_front == NULL)
	{
		printf("Nothing to pop\n");
	}
	else if (q->_front == q->_rear)
	{
		q->_front = NULL;
		q->_rear = NULL;
	}
	else
	{
		q->_front = q->_front->_next;
	}
}

void QueuePrint(Queue* q)
{
	assert(q);
	QNode* var = q->_front;
	while (var)
	{
		printf("%d ", var->_data);
		var = var->_next;
	}
	printf("\n");
}

// 获取队列头部元素 
QDataType QueueFront(Queue* q)
{
	assert(q);
	assert(q->_front);
	return q->_front->_data;
}

// 获取队列队尾元素 
QDataType QueueBack(Queue* q)
{
	assert(q);
	assert(q->_rear);
	return q->_rear->_data;
}

// 获取队列中有效元素个数 
int QueueSize(Queue* q)
{
	assert(q);
	QNode* p = q->_front;
	int sum = 0;
	while (p)
	{
		sum++;
		p = p->_next;
	}
	return sum;
}

// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
int QueueEmpty(Queue* q)
{
	assert(q);
	if (q->_front == NULL)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

// 销毁队列 
void QueueDestroy(Queue* q)
{
	assert(q);
	QNode* p1 = q->_front;
	QNode* p2 = q->_front;
	while (p1)
	{
		p2 = p1->_next;
		free(p1);
		p1 = p2;
	}
	q->_front = NULL;
	q->_rear = NULL;
}