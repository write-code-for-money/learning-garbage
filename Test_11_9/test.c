#define _CRT_SECURE_NO_WARNINGS 1

#include "queue.h"

int main()
{
	Queue q;
	QueueInit(&q);
	QueuePush(&q, 1);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	QueuePush(&q, 4);
	QueuePrint(&q);
	QueuePop(&q);
	QueuePrint(&q);

	/*QueuePop(&q);
	QueuePrint(&q);*/
	/*QueuePop(&q);
	QueuePop(&q);
	QueuePop(&q);*/
	printf("%d\n", QueueFront(&q));
	printf("%d\n", QueueBack(&q));
	printf("%d\n", QueueSize(&q));
	QueuePrint(&q);
	QueueDestroy(&q);
	QueuePrint(&q);

	return 0;
}