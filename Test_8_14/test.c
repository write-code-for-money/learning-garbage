#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>

int compare(void* e1, void* e2)
{
	return *(int*)e2 - *(int*)e1;
}

void change(char* left, char* right, int sz)
{
	int i = 0;
	char val = 0;
	for (i = 0; i < sz; i++)
	{
		val = *left;
		*left = *right;
		*right = val;
		left++;
		right++;
	}
}

void My_qsort(void* arr, int sz, int Type_sz, int (*p)(const void* e1, const void* e2))
{
	int i = 0;
	int j = 0;
	for (i = 0; i < sz - 1; i++)
	{
		for (j = 0; j < sz - i - 1; j++)
		{
			if (p((char*)arr + j * Type_sz, (char*)arr + (j + 1) * Type_sz) > 0)
			{
				change((char*)arr + j * Type_sz, (char*)arr + (j + 1) * Type_sz, Type_sz);
			}
		}
	}
}

void test()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int Type_sz = sizeof(arr[0]);
	My_qsort(arr, sz, Type_sz, compare);
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
}

int compare2(void* e1, void* e2)
{

}

void test2()
{
	float arr[] = { 2.1,5.4,6.5,9.7,8.4,2.9,7.4,1.7 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int Type_sz = sizeof(arr[0]);
	My_qsort(arr, sz, Type_sz, compare);
	for (int i = 0; i < sz; i++)
	{
		printf("%.1f ", arr[i]);
	}
	printf("\n");
}

int compare3(void* e1, void* e2)
{
	return strcmp((int*)e2, (int*)e1);
}

void test3()
{
	char arr[] = "abcdefg";
	int sz = sizeof(arr) / sizeof(arr[0]);
	int Type_sz = sizeof(arr[0]);
	My_qsort(arr, sz - 1, Type_sz, compare3);
	printf("%s\n", arr);
}

struct stu
{
	char name[20];
	int age;
};

int compare5(void* e1, void* e2)
{
	return strcmp(((struct stu*)e1)->name, ((struct stu*)e2)->name);
}

void test4()
{
	struct stu arr[] = { {"zhnagsan", 15}, {"lisi", 20}, {"wnagwu", 45} };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int Type_sz = sizeof(arr[0]);
	My_qsort(arr, sz, Type_sz, compare5);
	for (int i = 0; i < sz; i++)
	{
		printf("%s ", arr[i].name);
	}
	printf("\n");
}

int main()
{
	//test(); // 整形
	//test2();  //浮点型
	//test3();   //字符型
	test4();  //结构体类型
	return 0;
}