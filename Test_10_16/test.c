#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>

//int main()
//{
//	int* p = NULL;
//	//��̬�ڴ�����
//	p = (int*)malloc(0);
//
//	if (p == NULL)
//	{
//		printf("����ʧ�ܣ�\n");
//		exit(-1);
//	}
//	/*int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}*/
//	//�ڴ��ͷ�
//	free(p);
//	p = NULL;
//	return 0;
//}

//#include <stdlib.h>     /* malloc, calloc, realloc, free */
//
//int main()
//{
//	int* buffer1, * buffer2, * buffer3;
//	buffer1 = (int*)malloc(100 * sizeof(int));
//	buffer2 = (int*)calloc(100, 10 * sizeof(int));
//	buffer3 = (int*)realloc(buffer2, 500 * sizeof(int));
//	free(buffer1);
//	free(buffer3);
//	return 0;
//}

//#include <stdio.h>
//#include <stdlib.h>
//
//int main()
//{
//	int* p = NULL;
//	//��̬�ڴ�����
//	p = (int*)calloc(10, sizeof(int));
//
//	if (p == NULL)
//	{
//		printf("����ʧ�ܣ�\n");
//		exit(-1);
//	}
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i; 
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	//�ڴ��ͷ�
//	free(p);
//	p = NULL;
//	return 0;
//}

#include <stdio.h>
#include <stdlib.h>

int main()
{
	int* p = NULL;
	//��̬�ڴ�����
	p = (int*)malloc(10 * sizeof(int));

	if (p == NULL)
	{
		printf("����ʧ�ܣ�\n");
		exit(-1);
	}
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		*(p + i) = i;
	}

	// ����
	int* ptr = (int*)realloc(p, 20 * sizeof(int));
	if (ptr == NULL)
	{
		printf("realloc!\n");
		exit(-1);
	}
	int* t = p;

	p = ptr;
	ptr = NULL;
	for (i = 10; i < 20; i++)
	{
		*(p + i) = i;
	}

	for (i = 0; i < 20; i++)
	{
		printf("%d ", *(p + i));
	}
	//�ڴ��ͷ�
	free(p);
	p = NULL;
	return 0;
}