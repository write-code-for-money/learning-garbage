#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//#include <math.h>
//
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int i = 0;
//    int sum = 0;
//    while (n)
//    {
//        int x = n % 10;
//        if (x % 2 == 1)
//        {
//            x = 1;
//        }
//        else {
//            x = 0;
//        }
//        sum += x * pow(10, i++);
//        n = n / 10;
//    }
//    printf("%d\n", sum);
//    return 0;
//}

#include <stdio.h>

int compare(void* e1, void* e2)
{
    return *(int*)e2 - *(int*)e1;
}

//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int arr[100] = { 0 };
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    qsort(arr, n, 4, compare);
//    for (int i = 0; i < 5; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    printf("\n");
//    return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    while (scanf("%d", &n) == 1)
//    {
//        for (int i = 0; i < n; i++)
//        {
//            int sum = 1;
//            for (int j = 0; j <= i; j++)
//            {
//                printf("%d ", sum++);
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}

#include <stdio.h>

int main()
{
    int n = 0;
    while (scanf("%d", &n) == 1)
    {
        int val = n - 1;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (j == val)
                {
                    printf("*");
                }
                else {
                    printf(" ");
                }
            }
            val--;
            printf("\n");
        }
    }
    return 0;
}