#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int compare(void* e1, void* e2)
{
	return *(int*)e2 - *(int*)e1;
}

int compare2(void* e1, void* e2)
{
	return strcmp((char*)e2, (char*)e1);
}

void test()  //整形
{
	int arr[] = { 1,5,3,4,2,6,9,7,8,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int Type_sz = sizeof(arr[0]);
	qsort(arr, sz, Type_sz, compare);
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void test2()  //字符型
{
	char arr[] = { "abcdefghr" };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int Type_sz = sizeof(arr[0]);
	qsort(arr, sz - 1, Type_sz, compare2);
	for (int i = 0; i < sz - 1; i++)
	{
		printf("%c ", arr[i]);
	}
	printf("\n");
}

int compare3(void* e1, void* e2)
{
	return *(int*)e2 - *(int*)e1;
}


void test3() //浮点型
{
	float arr[] = { 2.5, 6.5, 2.1, 7.8, 4.9, 8.7, 9.5, 4.6, 7.5 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int Type_sz = sizeof(arr[0]);
	qsort(arr, sz, Type_sz, compare3);
	for (int i = 0; i < sz; i++)
	{
		printf("%.1f ", arr[i]);
	}
	printf("\n");
}

struct stu
{
	char name[20];
	int age;
};

int compare4(void* e1, void* e2)
{
	return strcmp(((struct stu*)e1)->name, ((struct stu*)e2)->name);
}

void test4()  // 结构体类型
{
	struct stu arr[] = { {"zhangsan", 15}, {"lisi", 18}, {"wnagwu", 21} };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int Type_sz = sizeof(arr[0]);
	qsort(arr, sz, Type_sz, compare4);
	for (int i = 0; i < sz; i++)
	{
		printf("%s ", arr[i].name);
	}
	printf("\n");
}

int main()
{
	test4();
	return 0;
}