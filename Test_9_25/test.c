#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//#include <string.h>
//
//int main()
//{
//	char arr[] = "abcdefg";
//	char arr2[] = "****";
//	printf("%s\n", strncpy(arr, arr2, 2));
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//
//int main()
//{
//	char arr[] = "abcdefg";
//	char arr2[] = "****";
//	printf("%s\n", strncpy(arr, arr2, 6));
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//
//char* my_strncpy(char* arr, char* arr2, int num)
//{
//	int i = 0;
//	for (i = 0; i < num; i++)
//	{
//		if (*(arr2 + i) != '\0')
//		{
//			*(arr + i) = *(arr2 + i);
//		}
//		else
//		{
//			break;
//		}
//	}
//	for (; i < num; i++)
//	{
//		*(arr + i) = '\0';
//	}
//	return arr;
//}
//
//int main()
//{
//	char arr[] = "abcdefg";
//	char arr2[] = "****";
//	printf("%s\n", my_strncpy(arr, arr2, 3));
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//
//int main()
//{
//	char arr[20] = "abcde\0iiiiiiiiiiii";
//	char arr2[] = "***";
//	printf("%s\n", strncat(arr, arr2, 6));
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//
//char* my_strncat(char* arr, char* arr2, int num)
//{
//	int sz = 0;
//	while (*(arr + sz) != '\0')
//	{
//		sz++;
//	}
//	for (int i = 0; i < num; i++)
//	{
//		if (*(arr2 + i) != '\0')
//		{
//			*(arr + sz) = *(arr2 + i);
//			sz++;
//		}
//		else
//		{
//			break;
//		}
//	}
//	*(arr + sz) = '\0';
//	return arr;
//}
//
//int main()
//{
//	char arr[20] = "abcdef";
//	char arr2[] = "****";
//	printf("%s\n", my_strncat(arr, arr2, 3));
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//
//int main()
//{
//	char arr[] = "abcdef";
//	char arr2[] = "abcdef";
//	printf("%d\n", strncmp(arr, arr2, 4));
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//
//int my_strncmp(char* arr, char* arr2, int num)
//{
//	for (int i = 0; i < num; i++)
//	{
//		if (*(arr + i) != *(arr2 + i))
//		{
//			return *(arr + i) - *(arr2 + i);
//		}
//	}
//	return 0;
//}
//
//int main()
//{
//	char arr[] = "abcde";
//	char arr2[] = "bcde";
//	printf("%d\n", my_strncmp(arr, arr2, 3));
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>

//int main()
//{
//	char arr[] = "abcdfabcdeabc";
//	char arr2[] = "fab";
//	printf("%s\n", strstr(arr, arr2));
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//
//int main()
//{
//	char arr[] = "abcdfabcdefabc";
//	char arr2[] = "fab";
//	char* p = strstr(arr, arr2);
//	printf("%s\n", p);
//	return 0;
//}

#include <stdio.h>
#include <string.h>

char* my_strstr(char* arr, char* arr2)
{
	char* p = arr;
	char* q = arr2;
	int sz = 0;
	int i = 0;
	while (*p != '\0' && *q != '\0')
	{
		if (*p == *q)
		{
			p++;
			q++;
		}
		else
		{
			p = arr + 1;
			arr = p;
			q = arr2;
		}
	}
	if (*p == '\0' && *q != '\0')
	{
		return NULL;
	}
	else
	{
		return arr;
	}
}

int main()
{
	char arr[] = "abcdfabcdefabc";
	char arr2[] = "efab";
	printf("%s\n", my_strstr(arr, arr2));
	return 0;
}