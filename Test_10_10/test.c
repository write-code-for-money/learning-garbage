#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//enum month
//{
//	day1,
//	day2,
//	day3
//};
//
//int main()
//{
//	printf("%d\n", sizeof(enum month));
//	return 0;
//}

//union num
//{
//	int i;
//	char ch;
//	short t;
//};

//union num
//{
//	int i;
//	char ch[5];
//	short t;
//}val2, val3;

//union num
//{
//	int i;
//	char ch;
//	short t;
//};
//
//int main()
//{
//	union num val = { 0 };
//	val.i = 6;
//	printf("%d\n", val.i);
//	printf("%d\n", val.ch);
//	printf("%d\n", sizeof(val));
//	printf("%d\n", sizeof(union num));
//	return 0;
//}

//union num
//{
//	int i;
//	char ch;
//	short t;
//};
//
//int main()
//{
//	union num val = { 0 };
//	val.ch = 'a';
//	val.i = 1;
//	val.t = 3;
//	printf("%p\n", &val.ch);
//	printf("%p\n", &val.i);
//	printf("%p\n", &val.t);
//	
//	return 0;
//}

//union num
//{
//	int i;
//	char ch;
//	short t;
//};
//
//int main()
//{
//	union num val = { 0 };
//	/*val.t = 5;
//	printf("%d\n", val.ch);*/
//	val.i = 10;
//	/*printf("%d\n", val.ch);*/
//	printf("%d\n", val.i);
//	val.t = 1;
//	/*printf("%d\n", val.t);*/
//	printf("%d\n", val.i);
//	return 0;
//}

union num
{
	int i;
	char ch[5];
};

int main()
{
	union num val = { 0 };
	printf("%d\n", sizeof(union num));
	return 0;
}