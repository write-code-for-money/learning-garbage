#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//struct book
//{
//	char book_name[20];
//	char author[20];
//	int price;
//	char num[15];
//}book_1, book_2;

//struct book
//{
//	char book_name[20];
//	char author[20];
//	int price;
//	char num[15];
//};

//struct
//{
//	char book_name[20];
//	char author[20];
//	int price;
//	char num[15];
//}book_1, book_2;

//struct node
//{
//	int data;
//	struct node* next;
//};
//
//int main()
//{
//	struct node book_1;
//	return 0;
//}

//struct node
//{
//	int data;
//	struct node* next;
//};
//struct book
//{
//	char book_name[20];
//	char author[20];
//	int price;
//	char num[15];
//};
//
//int main()
//{
//	struct book book_1 = { "高质量C/C++编程", "林锐", 99, "HX15935" };
//	struct book* p = &book_1;
//	printf("%s %s %d %s\n", p->book_name, p->author, p->price, p->num);
//	return 0;
//}

struct book1
{
	char x;
	int y;
	char z;
};

struct book2
{
	char x;
	struct book1 z;
	int y;
};

int main()
{
	printf("%d\n", sizeof(struct book1));
	printf("%d\n", sizeof(struct book2));

	return 0;
}