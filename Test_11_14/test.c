#define _CRT_SECURE_NO_WARNINGS 1

//用两个队列实现栈
//typedef struct Qstack {
//    int data;
//    struct Qstack* next;
//} MyStack;
//
//
//MyStack* myStackCreate() {
//    MyStack* head = (MyStack*)malloc(sizeof(MyStack));
//    head->next = NULL;
//    return head;
//}
//
//void myStackPush(MyStack* obj, int x) {
//    MyStack* new = (MyStack*)malloc(sizeof(MyStack));
//    new->data = x;
//    new->next = NULL;
//    MyStack* tail = obj;
//    while (tail->next)
//    {
//        tail = tail->next;
//    }
//    tail->next = new;
//}
//
//int myStackPop(MyStack* obj) {
//    MyStack* head1 = obj;
//    MyStack* tail = NULL;
//    //MyStack* head2 = obj;
//    while (obj->next)
//    {
//        tail = obj;
//        obj = obj->next;
//    }
//    tail->next = NULL;
//    int ret = obj->data;
//    free(obj);
//    return ret;
//}
//
//int myStackTop(MyStack* obj) {
//    MyStack* tail = obj;
//    while (tail->next)
//    {
//        tail = tail->next;
//    }
//    return tail->data;
//}
//
//bool myStackEmpty(MyStack* obj) {
//    return obj->next == NULL ? true : false;
//}
//
//void myStackFree(MyStack* obj) {
//    MyStack* tail = obj;
//    while (obj)
//    {
//        tail = obj->next;
//        free(obj);
//        obj = tail;
//    }
//}

//用两个栈实现队列
//typedef struct Squeue {
//    int* arr;
//    int top;
//} MyQueue;
//
//
//MyQueue* myQueueCreate() {
//    MyQueue* node = (MyQueue*)malloc(sizeof(MyQueue));
//    node->arr = (int*)malloc(sizeof(int) * 100);
//    node->top = -1;
//    return node;
//}
//
//void myQueuePush(MyQueue* obj, int x) {
//    obj->arr[++(obj->top)] = x;
//}
//
//int myQueuePop(MyQueue* obj) {
//    int arr[100] = { 0 };
//    for (int i = obj->top; i > 0; i--)
//    {
//        arr[i - 1] = obj->arr[i];
//    }
//    int ret = obj->arr[0];
//    memcpy(obj->arr, arr, 400);
//    obj->top--;
//    return ret;
//}
//
//int myQueuePeek(MyQueue* obj) {
//    return obj->arr[0];
//}
//
//bool myQueueEmpty(MyQueue* obj) {
//    return obj->top < 0 ? true : false;
//}
//
//void myQueueFree(MyQueue* obj) {
//    free(obj->arr);
//    free(obj);
//}


#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
//循环队列
typedef struct Cirqueue {
    int* arr;
    int top;
    int tail;
    int capacity;
} MyCircularQueue;


MyCircularQueue* myCircularQueueCreate(int k) {
    MyCircularQueue* node = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
    node->arr = (int*)malloc(sizeof(int) * (k + 1));
    node->top = 0;
    node->tail = 0;
    node->capacity = k + 1;
    return node;
}

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
    int len = sizeof(obj->arr) / sizeof(obj->arr[0]);
    if ((obj->tail + 1) % len == obj->top)
    {
        return false;
    }
    obj->arr[obj->tail++] = value;
    if (obj->tail == obj->capacity)
    {
        obj->tail %= obj->capacity;
    }
    return true;
}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {
    //int len = sizeof(obj->arr) / sizeof(obj->arr[0]);
    if (obj->tail == obj->top)
    {
        return false;
    }
    obj->top++;
    if (obj->top >= obj->capacity)
    {
        obj->top %= obj->capacity;
    }
    return true;
}

int myCircularQueueFront(MyCircularQueue* obj) {
    return obj->top == obj->tail ? -1 : obj->arr[obj->top];
}

int myCircularQueueRear(MyCircularQueue* obj) {
    //int len = sizeof(obj->arr) / sizeof(obj->arr[0]);
    int x = 0;
    if (obj->tail == 0)
    {
        x = obj->capacity - 1;
    }
    else
    {
        x = obj->tail - 1;
    }
    return obj->top == obj->tail ? -1 : obj->arr[x];
}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
    return obj->top == obj->tail ? true : false;
}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
    //int len = sizeof(obj->arr) / sizeof(obj->arr[0]);
    return (obj->tail + 1) % obj->capacity == obj->top ? true : false;
}

void myCircularQueueFree(MyCircularQueue* obj) {
    free(obj->arr);
    free(obj);
}
int main()
{
    int arr[10] = { 0 };
    //int sz = sizeof(arr) / sizeof(arr[0]);
    
    MyCircularQueue* obj = myCircularQueueCreate(3);
    bool param_1 = myCircularQueueEnQueue(obj, 1);
    bool param_2 = myCircularQueueEnQueue(obj, 2);
    bool param_3 = myCircularQueueEnQueue(obj, 3);
    return 0;
}
/**
 * Your MyCircularQueue struct will be instantiated and called as such:
 * MyCircularQueue* obj = myCircularQueueCreate(k);
 * bool param_1 = myCircularQueueEnQueue(obj, value);

 * bool param_2 = myCircularQueueDeQueue(obj);

 * int param_3 = myCircularQueueFront(obj);

 * int param_4 = myCircularQueueRear(obj);

 * bool param_5 = myCircularQueueIsEmpty(obj);

 * bool param_6 = myCircularQueueIsFull(obj);

 * myCircularQueueFree(obj);
*/

/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);

 * int param_2 = myQueuePop(obj);

 * int param_3 = myQueuePeek(obj);

 * bool param_4 = myQueueEmpty(obj);

 * myQueueFree(obj);
*/

/**
 * Your MyStack struct will be instantiated and called as such:
 * MyStack* obj = myStackCreate();
 * myStackPush(obj, x);

 * int param_2 = myStackPop(obj);

 * int param_3 = myStackTop(obj);

 * bool param_4 = myStackEmpty(obj);

 * myStackFree(obj);
*/