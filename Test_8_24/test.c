#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
//int* selfDividingNumbers(int left, int right, int* returnSize) {
//    static int arr[10000] = { 0 };
//    int i = 0;
//    int count = 0;
//    for (i = left; i <= right; i++)
//    {
//        int ret = i;
//        while (ret > 0)
//        {
//            int val = ret % 10;
//            if (val == 0)
//            {
//                break;
//            }
//            if (i % val != 0)
//            {
//                break;
//            }
//            ret /= 10;
//        }
//        if (ret == 0)
//        {
//            arr[count++] = i;
//        }
//    }
//    *returnSize = count;
//    return arr;
//}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* productExceptSelf(int* nums, int numsSize, int* returnSize) {
    static int arr[100000];
    int i = 0;
    int val = 1;
    for (i = 0; i < numsSize; i++)
    {
        arr[i] = val;
        val *= nums[i];
    }
    val = 1;
    for (i = numsSize - 1; i >= 0; i--)
    {
        arr[i] *= val;
        val *= nums[i];
    }
    *returnSize = numsSize;
    return arr;
}