#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stddef.h>

//struct student
//{
//	char name[10];
//	int age;
//	char card[10];
//};
//int main()
//{
//	printf("%d\n", (int)offsetof(struct student, name));
//	printf("%d\n", (int)offsetof(struct student, age));
//	printf("%d\n", (int)offsetof(struct student, card));
//	return 0;
//}

//struct student
//{
//	char name;
//	int age;
//	char card;
//};
//
//#define My_offsetof(type, member) (int)&(((struct student*)0)->member)
//
//int main()
//{
//	printf("%d\n", My_offsetof(struct student, name));
//	printf("%d\n", My_offsetof(struct student, age));
//	printf("%d\n", My_offsetof(struct student, card));
//	return 0;
//}


#include <stdio.h>
#define Change(i) (((i & 0xAAAAAAAA) >> 1) | ((i & 0x55555555) << 1))
int main()
{
	//写一个宏，可以将一个整数的二进制位的奇数位和偶数位交换。
	int i = 4; 
	int t = Change(i);
	printf("%d\n", t);
	return 0;
}