#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>

//int main()
//{
//    char arr[1000] = { 0 };
//    int i = 0;
//    int j = 0;
//    char tmp = 0;
//    while (scanf("%s", arr) != EOF)
//    {
//        int sz = strlen(arr);
//        for (i = 0; i < sz - 1; i++)
//        {
//            for (j = 0; j < sz - i - 1; j++)
//            {
//                if (arr[j] > arr[j + 1])
//                {
//                    tmp = arr[j];
//                    arr[j] = arr[j + 1];
//                    arr[j + 1] = tmp;
//                }
//            }
//        }
//        printf("%s\n", arr);
//    }
//
//    return 0;
//}

int Left(int t, int* nums)
{
    int i = t - 1;
    int sum = 0;
    while (i >= 0)
    {
        sum += nums[i--];
    }
    return sum;
}

int Right(int t, int* nums, int numsSize)
{
    int i = t + 1;
    int sum = 0;
    while (i < numsSize)
    {
        sum += nums[i++];
    }
    return sum;
}


int pivotIndex(int* nums, int numsSize) {
    int i = 0;
    int j = 0;
    if (numsSize == 1)
    {
        return 0;
    }
    for (i = 0; i < numsSize; i++)
    {
        int L_sum = Left(i, nums);
        int R_sum = Right(i, nums, numsSize);
        if (L_sum == R_sum)
        {
            return i;
        }
    }
    return -1;
}