#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int arr[n];
//    int min = 0;
//    int max = 0;
//    int sum = 0;
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//        if (arr[i] < 0)
//        {
//            min++;
//        }
//        if (arr[i] > 0)
//        {
//            max++;
//            sum += arr[i];
//        }
//    }
//    if (max != 0)
//    {
//        printf("%d %.1f", min, (float)((float)sum / max));
//    }
//    else {
//        printf("%d 0.0", min);
//    }
//    return 0;
//}
/**
 *
 * @param rotateArray int整型一维数组
 * @param rotateArrayLen int rotateArray数组长度
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int minNumberInRotateArray(int* rotateArray, int rotateArrayLen) {
    // write code here
//     int min = rotateArray[0];
//     for(int i = 0; i < rotateArrayLen; i++)
//     {
//         if(rotateArray[i] < min)
//         {
//             min = rotateArray[i];
//         }
//     }
//    return min;

    int i = 0;
    int j = rotateArrayLen - 1;
    int mid = 0;
    while (i < j)
    {
        mid = (i + j) / 2;
        if (rotateArray[mid] < rotateArray[j])
        {
            j = mid;
        }
        else if (rotateArray[mid] == rotateArray[j])
        {
            j--;
        }
        else {
            i = mid + 1;
        }
    }
    return rotateArray[i];
}
