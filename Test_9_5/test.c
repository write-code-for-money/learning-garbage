#define _CRT_SECURE_NO_WARNINGS 1

//int* printNumbers(int n, int* returnSize) {
//    // write code here
//    int sz = pow(10, n) - 1;
//    static int arr[100000];
//    *returnSize = sz;
//    for (int i = 0; i <= pow(10, n); i++)
//    {
//        arr[i] = i + 1;
//    }
//    return arr;
//}

#include <stdio.h>

int main()
{
    int year = 0;
    int month = 0;
    int day = 0;
    int x = 0;
    int sum = 0;
    scanf("%d %d %d", &year, &month, &day);
    if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))
    {
        for (int i = 1; i < month; i++)
        {
            switch (i)
            {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                x = 31;
                break;
            case 2:
                x = 29;
                break;
            default:
                x = 30;
                break;
            }
            sum += x;
        }
        sum += day;
        printf("%d\n", sum);
    }
    else {
        for (int i = 1; i < month; i++)
        {
            switch (i)
            {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                x = 31;
                break;
            case 2:
                x = 28;
                break;
            default:
                x = 30;
                break;
            }
            sum += x;
        }
        sum += day;
        printf("%d\n", sum);
    }
    return 0;
}