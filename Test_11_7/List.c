#define _CRT_SECURE_NO_WARNINGS 1

#include "List.h"

// 创建返回链表的头结点.
ListNode* ListCreate()
{
	ListNode* head = (ListNode*)malloc(sizeof(ListNode));
	head->_data = 0;
	head->_next = head;
	head->_prev = head;
	return head;
}


// 双向链表销毁
void ListDestory(ListNode* pHead)
{
	assert(pHead);
	ListNode* p1 = pHead->_next;
	ListNode* p2 = pHead;
	while (p1 != pHead)
	{
		p2 = p1;
		p1 = p1->_next;
		free(p2);
	}
}

// 双向链表打印
void ListPrint(ListNode* pHead)
{
	assert(pHead);
	ListNode* p = pHead->_next;
	while (p != pHead)
	{
		printf("%d ", p->_data);
		p = p->_next;
	}
	printf("\n");
}


ListNode* BuyNode(LTDataType x)
{
	ListNode* new = (ListNode*)malloc(sizeof(ListNode));
	new->_data = x;
	new->_next = NULL;
	new->_prev = NULL;
	return new;
}

// 双向链表在pos的前面进行插入
void ListInsert(ListNode* pos, LTDataType x)
{
	assert(pos);
	ListNode* p = BuyNode(x);
	p->_next = pos;
	p->_prev = pos->_prev;
	pos->_prev->_next = p;
	pos->_prev = p;
}

// 双向链表删除pos位置的节点
void ListErase(ListNode* pos)
{
	assert(pos);
	//assert(pos != pos->_next);
	if (pos == pos->_next)
	{
		return;
	}
	pos->_prev->_next = pos->_next;
	pos->_next->_prev = pos->_prev;
	free(pos);
}

// 双向链表尾插
void ListPushBack(ListNode* pHead, LTDataType x)
{
	/*ListNode* p = BuyNode(x);
	ListNode* ret = pHead->_prev;
	p->_next = pHead;
	ret->_next = p;
	p->_prev = ret;
	pHead->_prev = p;*/
	ListInsert(pHead, x);
}

// 双向链表尾删
void ListPopBack(ListNode* pHead)
{
	assert(pHead);
	/*ListNode* p = pHead->_prev;
	p->_prev->_next = pHead;
	pHead->_prev = p->_prev;
	free(p);*/
	ListErase(pHead->_prev);
}

// 双向链表头插
void ListPushFront(ListNode* pHead, LTDataType x)
{
	assert(pHead);
	/*ListNode* p = BuyNode(x);
	p->_next = pHead->_next;
	pHead->_next->_prev = p;
	p->_prev = pHead;
	pHead->_next = p;*/
	ListInsert(pHead->_next, x);
}
// 双向链表头删
void ListPopFront(ListNode* pHead)
{
	assert(pHead);
	/*ListNode* p = pHead->_next;
	p->_next->_prev = pHead;
	pHead->_next = p->_next;
	free(p);*/
	ListErase(pHead->_next);
}


// 双向链表查找
ListNode* ListFind(ListNode* pHead, LTDataType x)
{
	assert(pHead);
	ListNode* p = pHead->_next;
	while (p != pHead)
	{
		if (p->_data == x)
		{
			return p;
		}
		p = p->_next;
	}
	return NULL;
}

void ListModify(ListNode* pHead, LTDataType x)
{
	ListNode* p = ListFind(pHead, x);
	scanf("%d", &p->_data);
}

