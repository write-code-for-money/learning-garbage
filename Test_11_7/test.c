#define _CRT_SECURE_NO_WARNINGS 1

#include "List.h"


void test()
{
	ListNode* p = ListCreate();
	ListInsert(p, 5);
	ListInsert(p, 2);
	ListInsert(p, 6);
	ListPrint(p);
	ListErase(p->_next);
	ListPrint(p);
	ListErase(p->_prev);
	ListPrint(p);

	ListDestory(p);
}

void test1()
{
	ListNode* p = ListCreate();
	ListErase(p);
	ListPushBack(p, 1);
	ListPushBack(p, 2);
	ListPushBack(p, 3);
	ListPrint(p);

	ListPopBack(p);
	ListPopBack(p);

	ListPrint(p);

}

void test2()
{
	ListNode* p = ListCreate();
	ListPushFront(p, 1);
	ListPushFront(p, 2);
	ListPushFront(p, 3);
	ListPrint(p);
	ListModify(p, 2);
	ListPrint(p);

	/*ListInsert(ListFind(p, 2), 4);
	ListPrint(p);*/

	/*ListPopFront(p);
	ListPopFront(p);
	ListPrint(p);

	ListPopFront(p);*/
	//ListFind(p, 2);
	

}

int main()
{
	//test();
	//test1();
	test2();
	return 0;
}