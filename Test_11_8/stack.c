#define _CRT_SECURE_NO_WARNINGS 1

#include "stack.h"

// 初始化栈 
void StackInit(Stack* ps)
{
	assert(ps);
	ps->_a = (STDataType*)malloc(capacityNum * sizeof(STDataType));
	ps->_capacity = capacityNum;
	ps->_top = -1;
}

// 入栈 
void StackPush(Stack* ps, STDataType data)
{
	assert(ps);
	STDataType* p = ps->_a;
	if (ps->_top == ps->_capacity - 1)
	{
		p = realloc(p, sizeof(STDataType) * (ps->_capacity + capacityNum));
		ps->_a = p;
		ps->_capacity += capacityNum;
	}
	ps->_a[++ps->_top] = data;
}

// 出栈 
void StackPop(Stack* ps)
{
	if (ps->_top < 0)
	{
		printf("Nothing to pop\n");
		return;
	}
	ps->_top--;
}

// 获取栈顶元素 
STDataType StackTop(Stack* ps)
{
	assert(ps);
	return ps->_a[ps->_top];
}

void StackPrint(Stack* ps)
{
	assert(ps);
	if (ps->_top < 0)
	{
		printf("Nothing to print\n");
		return;
	}
	for (int i = 0; i <= ps->_top; i++)
	{
		printf("%d ", ps->_a[i]);
	}
	printf("\n");
}

// 获取栈中有效元素个数 
int StackSize(Stack* ps)
{
	assert(ps);
	return ps->_top + 1;
}

// 检测栈是否为空，如果为空返回非零结果，如果不为空返回0 
int StackEmpty(Stack* ps)
{
	assert(ps);
	return ps->_top < 0 ? 1 : 0;
}

// 销毁栈 
void StackDestroy(Stack* ps)
{
	assert(ps);
	ps->_capacity = 0;
	ps->_top = -1;
	free(ps->_a);
}