#define _CRT_SECURE_NO_WARNINGS 1

#include "stack.h"

void Test()
{
	Stack st;
	StackInit(&st);
	StackPush(&st, 1);
	StackPush(&st, 2);
	StackPush(&st, 3);
	StackPush(&st, 4);
	StackPush(&st, 5);
	StackPush(&st, 6);
	StackPrint(&st);
	//printf("%d", StackSize(&st));
	//printf("%d", StackEmpty(&st)); 
	StackPop(&st);
	StackPop(&st);
	StackPop(&st);
	/*StackPop(&st);
	StackPop(&st);
	StackPop(&st);
	StackPop(&st);*/
	StackPrint(&st);
	//printf("%d\n", StackEmpty(&st));
	//printf("%d", StackTop(&st));
	//printf("%d", StackSize(&st));
	StackDestroy(&st);
	StackPrint(&st);

}

int main()
{
	Test();
	return 0;
}