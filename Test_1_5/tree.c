#define _CRT_SECURE_NO_WARNINGS 1

#include "tree.h"


// 通过前序遍历的数组"ABD##E#H##CF##G##"构建二叉树
BTNode* BinaryTreeCreate(BTDataType* a, int n, int* pi)
{
	if (a[*pi] == '#')
	{
		(*pi)++;
		return NULL;
	}
	BTNode* root = (BTNode*)malloc(sizeof(BTNode));
	root->_data = a[(*pi)++];
	root->_left = BinaryTreeCreate(a, n, pi);
	root->_right = BinaryTreeCreate(a, n, pi);
	return root;
}

// 二叉树销毁
void BinaryTreeDestory(BTNode** root)
{
	if ((*root) == NULL)
	{
		return;
	}
	BinaryTreeDestory(&(*root)->_left);
	BinaryTreeDestory(&(*root)->_right);
	free(*root);
}

// 二叉树节点个数
int BinaryTreeSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	return BinaryTreeSize(root->_left) + BinaryTreeSize(root->_right) + 1;
}

// 二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	if (root->_left == NULL || root->_right == NULL)
	{
		return 1;
	}
	return BinaryTreeLeafSize(root->_left) + BinaryTreeLeafSize(root->_right);
}


// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	if (root == NULL)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}
	return BinaryTreeLevelKSize(root->_left, k-1) + BinaryTreeLevelKSize(root->_right, k-1);
}

// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
	{
		return NULL;
	}
	if (root->_data == x)
	{
		return root;
	}
	if (BinaryTreeFind(root->_left, x))
	{
		return BinaryTreeFind(root->_left, x);
	}
	else
	{
		return BinaryTreeFind(root->_right, x);
	}
}

// 二叉树前序遍历 
void BinaryTreePrevOrder(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}
	printf("%c ", root->_data);
	BinaryTreePrevOrder(root->_left);
	BinaryTreePrevOrder(root->_right);
}

// 二叉树中序遍历
void BinaryTreeInOrder(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}
	BinaryTreeInOrder(root->_left);
	printf("%c ", root->_data);
	BinaryTreeInOrder(root->_right);
}

// 二叉树后序遍历
void BinaryTreePostOrder(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}
	BinaryTreePostOrder(root->_left);
	BinaryTreePostOrder(root->_right);
	printf("%c ", root->_data);
}

void Queuepush(Queue* que, BTNode* root)
{
	if (root->_left != NULL)
	{
		Qnode* node1 = (Qnode*)malloc(sizeof(Qnode));
		node1->_data = root->_left->_data;
		node1->_next = NULL;
		node1->base = root->_left;
		que->tail->_next = node1;
		que->tail = node1;
	}
	if (root->_right != NULL)
	{
		Qnode* node2 = (Qnode*)malloc(sizeof(Qnode));
		node2->_data = root->_right->_data;
		node2->_next = NULL;
		node2->base = root->_right;
		que->tail->_next = node2;
		que->tail = node2;
	}
}


void Queuepop(Queue* que)
{
	printf("%c ", que->front->_data);
	Queuepush(que, que->front->base);
	que->front = que->front->_next;
}
// 层序遍历
void BinaryTreeLevelOrder(BTNode* root)
{
	Queue que;
	que.front = que.tail = NULL;
	Qnode* node = (Qnode*)malloc(sizeof(Qnode));
	node->base = root;
	node->_data = root->_data;
	node->_next = NULL;
	que.front = que.tail = node;
	while (que.front)
	{
		Queuepop(&que);
	}
}


int Queuepush2(Queue* que, BTNode* root, int* sum, int sz)
{
	if (root->_left != NULL)
	{
		Qnode* node1 = (Qnode*)malloc(sizeof(Qnode));
		node1->_data = root->_left->_data;
		node1->_next = NULL;
		node1->base = root->_left;
		que->tail->_next = node1;
		que->tail = node1;
		(*sum)++;
	}
	if (root->_left == NULL || root->_right == NULL)
	{
		if ((*sum) == sz - 1)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
	
	if (root->_right != NULL)
	{
		Qnode* node2 = (Qnode*)malloc(sizeof(Qnode));
		node2->_data = root->_right->_data;
		node2->_next = NULL;
		node2->base = root->_right;
		que->tail->_next = node2;
		que->tail = node2;
		(*sum)++;
	}
	return 1;
}
int Queuepop2(Queue* que, int* sum, int sz)
{
	int ret = Queuepush2(que, que->front->base, sum, sz);
	que->front = que->front->_next;
	return ret;
}
int BinaryTreeLevelOrder2(BTNode* root, int sz)
{
	Queue que;
	que.front = que.tail = NULL;
	Qnode* node = (Qnode*)malloc(sizeof(Qnode));
	node->base = root;
	node->_data = root->_data;
	node->_next = NULL;
	que.front = que.tail = node;
	int sum = 0;
	while (que.front)
	{
		int ret = Queuepop2(&que, &sum, sz);
		if (ret == 0)
		{
			return 0;
		}
		else if (ret == -1)
		{
			return -1;
		}
	}
}
// 判断二叉树是否是完全二叉树
int BinaryTreeComplete(BTNode* root)
{
	if (root == NULL)
	{
		return 1;
	}
	int sz = BinaryTreeSize(root);
	int ret = BinaryTreeLevelOrder2(root, sz);
	return ret;
}