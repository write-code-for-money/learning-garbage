#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
//int main() {
//    int n = 0;
//    while (scanf("%d", &n) == 1)
//    {
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (j >= n - i - 1)
//                {
//                    printf("* ");
//                }
//                else {
//                    printf("  ");
//                }
//            }
//            printf("\n");
//        }
//    }
//
//    return 0;
//}

#include <stdio.h>

int main() {
    int n = 0;
    int arr[100000] = { 0 };
    int k = 0;
    scanf("%d", &n);
    int i = 0;
    int max = 0;
    for (i = 0; i < n; i++)
    {
        scanf("%d", &k);
        arr[k] = k;
        if (k > max)
        {
            max = k;
        }
    }
    for (int i = 0; i <= max; i++)
    {
        if (arr[i])
        {
            printf("%d ", arr[i]);
        }
    }
    return 0;
}