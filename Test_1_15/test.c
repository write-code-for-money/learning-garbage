#define _CRT_SECURE_NO_WARNINGS 1

//ƽ�������
int lenth(struct TreeNode* root)
{
    if (root == NULL)
    {
        return 0;
    }
    int left = lenth(root->left);
    int right = lenth(root->right);
    return left > right ? left + 1 : right + 1;
}

bool isBalanced(struct TreeNode* root) {
    if (root == NULL)
    {
        return true;
    }
    int left = lenth(root->left);
    int right = lenth(root->right);
    return abs(left - right) <= 1 && isBalanced(root->left) && isBalanced(root->right);
}