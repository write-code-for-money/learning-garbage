#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <math.h>

void HeapCreat(int* a, int n)
{
	int j = 0;
	while (pow(2, j) < n)
	{
		for (int i = 0; i <= (n - 2) / 2; i++)
		{
			int max = a[i * 2 + 1] > a[i * 2 + 2] ? i * 2 + 1 : i * 2 + 2;
			if (a[i] < a[max])
			{
				int tmp = a[i];
				a[i] = a[max];
				a[max] = tmp;
			}
		}
		j++;
	}
	
}

void Heap_Down(int* a, int n)
{
	int i = 0;
	while (i * 2 + 1 < n)
	{
		int max = i * 2 + 1;
		if (a[i * 2 + 1] < a[i * 2 + 2] && i * 2 + 2 < n)
		{
			max = i * 2 + 2;
		}
		if (a[i] < a[max])
		{
			int tmp = a[i];
			a[i] = a[max];
			a[max] = tmp;
		}
		i = max;
	}
	
}

void HeapSort(int* a, int n)
{
	HeapCreat(a, n); //建立一个大堆
	while (n > 0)
	{
		int tmp = a[0];
		a[0] = a[n - 1];
		a[n - 1] = tmp;
		n--;
		Heap_Down(a, n);
	}
}


int main()
{
	int arr[] = { 10,9,8,7,6,12,3,45,4,5,100 };
	HeapSort(arr, sizeof(arr)/sizeof(arr[0]));
	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
	return 0;
}