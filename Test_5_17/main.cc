#include <iostream>


struct MyStruct {
    int a;
    double b;
    char c;
};


template <typename T, std::size_t N>
void printSizeAndLength(const T (&arr)[N]) {
    std::cout << "Size of arr in function: " << sizeof(arr) << std::endl; // 计算数组的大小
    std::cout << "Length of arr: " << strlen(arr) << std::endl; // 计算字符串的长度
}




int main() {
    char str[] = "Hello, world!";
    std::cout << "Size of str in main: " << sizeof(str) << std::endl; // 计算整个字符数组的大小
    printSizeAndLength(str);
}

//int main()
//{
//    MyStruct ms;
//    int arr[10];
//    std::cout << sizeof(ms) << std::endl;
//    std::cout << sizeof(arr) << std::endl;
//	return 0;
//}