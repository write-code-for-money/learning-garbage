#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#define  max  10

int Back_print(int j, int i)
{
	if (j == 1 || j == i)
	{
		return 1;
	}
	else
	{
		return Back_print(j - 1, i - 1) + Back_print(j, i - 1);
	}
}

int main()
{
	int i = 0;
	int j = 0;
	for (i = 1; i <= max; i++)
	{
		for (j = 1; j <= i; j++)
		{
			int ret = Back_print(j, i);
			printf("%d ", ret);
		}
		printf("\n");
	}
	return 0;
}

/**
 *
 * @param data int整型一维数组
 * @param dataLen int data数组长度
 * @param k int整型
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int Total(int* data, int mid, int k, int dataLen)
{
    int t = mid - 1;
    int sum = 0;
    while (1)
    {
        if (t >= 0 && data[t] == k)
        {
            sum++;
            t--;
        }
        else {
            break;
        }
    }
    while (1)
    {
        if (mid < dataLen && data[mid] == k)
        {
            sum++;
            mid++;
        }
        else {
            break;
        }
    }
    return sum;
}

int GetNumberOfK(int* data, int dataLen, int k) {
    // write code here
    int left = 0;
    int right = dataLen - 1;
    int mid = 0;
    int sum = 0;
    if (dataLen == 1)
    {
        if (data[0] == k)
        {
            return 1;
        }
        else {
            return 0;
        }
    }
    while (left < right)
    {
        mid = (left + right) / 2;
        if (data[mid] == k || data[left] == k || data[right] == k)
        {
            int ret = Total(data, mid, k, dataLen);
            return ret;
        }
        if (data[mid] > k)
        {
            right = mid - 1;
        }
        if (data[mid] < k)
        {
            left = mid + 1;
        }
    }
    return 0;
}

//int convertInteger(int A, int B) {
//    int sum = 0;
//    int x = A ^ B;
//    for (int i = 0; i < 32; i++)
//    {
//        if (((x >> i) & 1) == 1)
//        {
//            sum++;
//        }
//    }
//    return sum;
//}