#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//#include <string.h>

//int main()
//{
//	char arr[] = { 'a', 'b', 'c'};
//	printf("%d\n", strlen(arr));
//	return 0;
//}

//int main()
//{
//	if (strlen("abc") - strlen("abcde") > 0)
//	{
//		printf(">\n");
//	}
//	else
//	{
//		printf("<\n");
//	}
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//
//#define max_length 100
//
//size_t my_strlen(const char* arr)
//{
//	int i = 0;
//	while (arr[i] != '\0')
//	{
//		i++;
//	}
//	return i;
//}
//
//int main()
//{
//	char arr[max_length] = { 0 };
//	gets(arr);
//	printf("%d\n", my_strlen(arr));
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>

//int main()
//{
//	char arr[] = "**********";
//	char arr2[] = "hello";
//	strcpy(arr, arr2);
//	printf("%s\n", arr);
//	return 0;
//}


//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char arr[] = "**********";
//	char arr2[] = {'a', 'b', 'c'};
//	strcpy(arr, arr2);
//	printf("%s\n", arr);
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char* p = "***********";
//	char arr2[] = "abc";
//	strcpy(p, arr2);
//	printf("%s\n", p);
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//#include <assert.h>

//char* my_strcpy(char* arr, const char* arr2)
//{
//	assert(arr);
//	assert(arr2);
//	char* p1 = arr;
//	char* p2 = arr2;
//	while (*p2 != '\0')
//	{
//		*p1++ = *p2++;
//	}
//	*p1 = '\0';
//	return arr;
//}
//#include <stdio.h>
//#include <string.h>
//#include <assert.h>
//char* my_strcat(char* arr, const char* arr2)
//{
//	assert(arr);
//	assert(arr2);
//	char* p = arr;
//	while (*p != '\0')
//	{
//		p++;
//	}
//	char* q = arr2;
//	while (*q != '\0')
//	{
//		*p++ = *q++;
//	}
//	*p = '\0';
//	return arr;
//}
#include <stdio.h>
#include <string.h>
#include <assert.h>

int my_strcmp(const char* str1, const char* str2)
{
	assert(str1 && str2);
	while (*str1 == *str2 && *str1 != '\0')
	{
		str1++;
		str2++;
	}
	return *str1 - *str2;
}

int main()
{
	char arr[100] = "abcd";
	char arr2[] = "bcde";
	if (my_strcmp(arr, arr2) > 0)
	{
		printf("arr > arr2\n");
	}
	else if(my_strcmp(arr, arr2) < 0)
	{
		printf("arr < arr2\n");
	}
	else
	{
		printf("arr = arr2\n");
	}
	return 0;
}