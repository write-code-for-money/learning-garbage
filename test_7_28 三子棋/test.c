#define _CRT_SECURE_NO_WARNINGS 1

#include "head.h"

void game()
{
	int i = 0;
	int j = 0;
	char ch = 0;
	int val = Row > Col ? Col : Row;
	char arr[Row][Col] = { 0 };
	init_chess(arr);   //初始化
	input_chess(arr);  //打印
	while (1)
	{
		player_chess(arr);  //玩家下
		i++;
		if (i >= val)
		{
			ch = think_chess(arr);
			if (ch == '*')
			{
				printf("玩家赢\n");
				break;
			}
			if (ch == '#')
			{
				printf("电脑赢\n");
				break;
			}
			if (ch == '@')
			{
				printf("平局\n");
				break;
			}
		}
		computer_chess(arr); // 电脑下
		j++;
		if (j >= val)
		{
			ch = think_chess(arr);
			if (ch == '*')
			{
				printf("玩家赢\n");
				break;
			}
			if (ch == '#')
			{
				printf("电脑赢\n");
				break;
			}
			if (ch == '@')
			{
				printf("平局\n");
				break;
			}
		}
	}
}

void menu()
{
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	printf("~~~~~~~~~1.start~~~~~~~~~\n");
	printf("~~~~~~~~~0.exit ~~~~~~~~~\n");
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~\n");
}


int main()
{
	int i = 1;
	srand((unsigned int)time(NULL));
	while (i)
	{
		menu();
		printf("请输入你的选择\n");
		scanf("%d", &i);
		switch (i)
		{
		case 1:
			game();
			break;
		case 0:
			printf("成功退出！\n");
			break;
		default:
			printf("输入错误，请重新输入\n");
			break;
		}
	}
	return 0;
}