#pragma once

#include "head.h"

void init_chess(char arr[Row][Col]) // 初始化棋盘
{
	int i = 0;
	int j = 0;
	for (i = 0; i < Row; i++)
	{
		for (j = 0; j < Col; j++)
		{
			arr[i][j] = ' ';
		}
	}
}

void input_chess(char arr[Row][Col]) //打印棋盘
{
	int i = 0;
	int j = 0;
	for (i = 0; i < Row; i++)
	{
		for (j = 0; j < Col; j++)
		{
			printf(" %c ", arr[i][j]);
			if (j != Col - 1)
			{
				printf("|");
			}
		}
		printf("\n");
		if (i != Row - 1)
		{
			for (j = 0; j < Col; j++)
			{
				printf("---");
				if (j != Col - 1)
				{
					printf("|");
				}
			}
			printf("\n");
		}
	}
}

void player_chess(char arr[Row][Col])  //玩家下棋
{
	int i = 0;
	int j = 0;
	while (1)
	{
		printf("你下：\n");
		scanf("%d %d", &i, &j);
		if (i > 0 && i <= Row && j > 0 && j <= Col)
		{
			if (arr[i - 1][j - 1] == ' ')
			{
				arr[i - 1][j - 1] = '*';
				break;
			}
			else
			{
				printf("已被占用，请重新下棋：>\n");
			}
		}
		else
		{
			printf("输入的坐标不合法，请重新输入\n");
		}
	}
	input_chess(arr);
}

void computer_chess(char arr[Row][Col])
{
	int i = 0;
	int j = 0;
	printf("电脑下：>\n");
	while (1)
	{
		i = rand() % Row;
		j = rand() % Col;
		if (arr[i][j] == ' ')
		{
			arr[i][j] = '#';
			break;
		}
	}
	input_chess(arr);
}

char think_chess(char arr[Row][Col])  //输赢判断函数
{
	int i = 0;
	int j = 0;
	int num = Row > Col ? Col : Row;
	int flag = 0;
	int sum1 = 0;
	int sum2 = 0;
	int sum3 = 0;
	int sum4 = 0;
	
	j = Col - 1;
	while (i < Row && j >= 0)
	{
		if (arr[i][j] == '*')
		{
			sum1++;
			if (sum1 == num)
			{
				return '*';
			}
		}
		if (arr[i][j] == '#')
		{
			sum2++;
			if (sum2 == num)
			{
				return '#';
			}
		}
		i++;
		j--;
	}
	
	//
	sum1 = 0;
	sum2 = 0;
	for (i = 0; i < num; i++)
	{
		if (arr[i][i] == '*')
		{
			sum1++;
			if (sum1 == num)
			{
				return '*';
			}
		}
		if (arr[i][i] == '#')
		{
			sum2++;
			if (sum2 == num)
			{
				return '#';
			}
		}
	}
	
	for (i = 0; i < Row; i++)
	{
		sum1 = 0;
		sum2 = 0;
		sum3 = 0;
		sum4 = 0;
		for (j = 0; j < Col; j++)
		{
			if (arr[i][j] == ' ')
			{
				flag = 1;
			}
			
			//行
			if (arr[i][j] == '*')
			{
				sum1++;
				if (sum1 == num)
				{
					return '*';
				}
			}
			if (arr[i][j] == '#')
			{
				sum2++;
				if (sum2 == num)
				{
					return '#';
				}
			}
			//列
			if (arr[j][i] == '*')
			{
				sum3++;
				if (sum3 == num)
				{
					return '*';
				}
			}
			if (arr[j][i] == '#')
			{
				sum4++;
				if (sum4 == num)
				{
					return '#';
				}
			}
		}
		
	}
	if (flag == 0)
	{
		return '@';
	}
	return '1';
}