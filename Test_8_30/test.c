#define _CRT_SECURE_NO_WARNINGS 1

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
//int* masterMind(char* solution, char* guess, int* returnSize) {
//    static int answer[2] = { 0 };
//    int sum = 0;
//    for (int i = 0; i < 4; i++)
//    {
//        if (solution[i] == guess[i])
//        {
//            sum++;
//            solution[i] = '0';
//            guess[i] = '0';
//        }
//    }
//    answer[0] = sum;
//    sum = 0;
//    for (int i = 0; i < 4; i++)
//    {
//        if (guess[i] != '0')
//        {
//            for (int j = 0; j < 4; j++)
//            {
//                if (solution[j] == guess[i] && solution[j] != '0')
//                {
//                    sum++;
//                    solution[j] = '0';
//                    break;
//                }
//            }
//        }
//    }
//    answer[1] = sum;
//    *returnSize = 2;
//    return answer;
//}

/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 *
 * @param numbers int整型一维数组
 * @param numbersLen int numbers数组长度
 * @param target int整型
 * @return int整型一维数组
 * @return int* returnSize 返回数组行数
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int compare(void* e1, void* e2)
{
    return *(int*)e1 - *(int*)e2;
}

int* twoSum(int* numbers, int numbersLen, int target, int* returnSize) {
    // write code here
    int arr_2[100000] = { 0 };
    for (int i = 0; i < numbersLen; i++)
    {
        arr_2[i] = numbers[i];
    }
    qsort(numbers, numbersLen, sizeof(numbers[0]), compare);
    int left = 0;
    int right = numbersLen - 1;
    static int arr[2] = { 0 };
    int count = 0;
    while (right >= left)
    {
        if (numbers[right] + numbers[left] == target)
        {
            break;
        }
        if (numbers[right] + numbers[left] > target)
        {
            right--;
        }
        else {
            left++;
        }
    }
    for (int i = 0; i < numbersLen; i++)
    {
        if (arr_2[i] == numbers[left] || arr_2[i] == numbers[right])
        {
            arr[count++] = i + 1;
        }
    }
    *returnSize = 2;
    return arr;
}