#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int val = 0;
//    int sum = 0;
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &val);
//        sum ^= val;
//    }
//    printf("%d\n", sum);
//    return 0;
//}

int findPeakElement(int* nums, int numsLen) {
    // write code here
    for (int i = 0; i < numsLen; i++)
    {
        if (i == 0)
        {
            if (nums[i + 1] < nums[i])
            {
                return i;
            }
        }
        else if (i == numsLen - 1)
        {
            if (nums[i - 1] < nums[i])
            {
                return i;
            }
        }
        else
        {
            if (nums[i] > nums[i + 1] && nums[i] > nums[i - 1])
            {
                return i;
            }
        }
    }
    return -1;
}