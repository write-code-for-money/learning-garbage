#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,1,2,3,4,7 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int i = 0; 
//	int j = 0;
//	for (i = 0; i < sz; i++)
//	{
//		for (j = 0; j < sz; j++)
//		{
//			if (arr[i] == arr[j] && i != j)
//			{
//				break;
//			}
//		}
//		if (j == sz)
//		{
//			printf("%d\n", arr[i]);
//		}
//	}
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//
//char* my_strncpy(char* arr, char* arr2, int num)
//{
//	int i = 0;
//	for (i = 0; i < num; i++)
//	{
//		if (*(arr2 + i) != '\0')
//		{
//			*(arr + i) = *(arr2 + i);
//		}
//		else
//		{
//			break;
//		}
//	}
//	for (; i < num; i++)
//	{
//		*(arr + i) = '\0';
//	}
//	return arr;
//}
//
//int main()
//{
//	char arr[] = "abcdefg";
//	char arr2[] = "****";
//	printf("%s\n", my_strncpy(arr, arr2, 3));
//	return 0;
//}


//#include <stdio.h>
//#include <string.h>
//
//char* my_strncat(char* arr, char* arr2, int num)
//{
//	int sz = 0;
//	while (*(arr + sz) != '\0')
//	{
//		sz++;
//	}
//	for (int i = 0; i < num; i++)
//	{
//		if (*(arr2 + i) != '\0')
//		{
//			*(arr + sz) = *(arr2 + i);
//			sz++;
//		}
//		else
//		{
//			break;
//		}
//	}
//	*(arr + sz) = '\0';
//	return arr;
//}
//
//int main()
//{
//	char arr[20] = "abcdef";
//	char arr2[] = "****";
//	printf("%s\n", my_strncat(arr, arr2, 3));
//	return 0;
//}

//#include <stdio.h>
//#include <assert.h>
//#include <stdlib.h>
//int my_atoi(const char* arr)
//{
//	assert(arr);
//	int i = 0;
//	int sum = 0;
//	while (*(arr + i) != '\0')
//	{
//		if (*(arr + i) == ' ')
//		{
//			i++;
//		}
//		else if(*(arr + i) == '-' || *(arr + i) == '+')
//		{
//			if (*(arr + i) == '-')
//			{
//				i++;
//				while (*(arr + i) <= '9' && *(arr + i) >= '0')
//				{
//					int t = *(arr + i) - '0';
//					sum = sum * 10 + t;
//					i++;
//				}
//				return sum * -1;
//			}
//			else
//			{
//				i++;
//				while (*(arr + i) <= '9' && *(arr + i) >= '0')
//				{
//					int t = *(arr + i) - '0';
//					sum = sum * 10 + t;
//					i++;
//				}
//				return sum;
//			}
//		}
//		else if (*(arr + i) <= '9' && *(arr + i) >= '0')
//		{
//			while (*(arr + i) <= '9' && *(arr + i) >= '0')
//			{
//				int t = *(arr + i) - '0';
//				sum = sum * 10 + t;
//				i++;
//			}
//			return sum;
//		}
//		else
//		{
//			return 0;
//		}
//	}
//	
//}
//
//int main()
//{
//	char arr[] = " 12345-abc";
//	char arr2[] = " 12300abc";
//	int i = my_atoi(arr);
//	int j = my_atoi(arr2);
//	printf("%d\n", i);
//	printf("%d\n", j);
//	printf("%d\n", i - j);
//	return 0;
//}

//int main()
//{
//	char arr[] = " -12345-abc";
//	char arr2[] = " 12300abc";
//	int i = atoi(arr);
//	int j = atoi(arr2);
//	printf("%d\n", i);
//	printf("%d\n", j);
//	printf("%d\n", i - j);
//	return 0;
//}