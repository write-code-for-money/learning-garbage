#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>

int Back_Print(int Rows, int Cols)
{
	if (Cols == 0 || Rows == Cols)
	{
		return 1;
	}
	else
	{
		return Back_Print(Rows - 1, Cols) + Back_Print(Rows - 1, Cols - 1);
	}
}

int main()
{
	//杨辉三角的递归打印
	int num = 0;
	scanf("%d", &num);
	int blank = 0;
	for (int i = 0; i < num; i++)
	{
		blank = num - i - 1;
		while (blank--)
		{
			printf(" ");
		}
		for (int j = 0; j <= i; j++)
		{
			printf("%2d ", Back_Print(i, j));
		}
		printf("\n");
	}
	return 0;
}

//#include <stdio.h>
//#include <assert.h>
//
//#define Rows 100
//#define Cols 100
//
//int main()
//{
//	int arr1[Rows][Cols] = { 0 };
//	int num = 0;
//	scanf("%d", &num);
//	assert(num < Rows);
//	for (int i = 0; i < num; i++)
//	{
//		for (int j = 0; j <= i; j++)
//		{
//			if (j == 0 || j == i)
//			{
//				arr1[i][j] = 1;
//			}
//			else
//			{
//				arr1[i][j] = arr1[i - 1][j - 1] + arr1[i - 1][j];
//			}
//		}
//	}
//	for (int i = 0; i < num; i++)
//	{
//		int blank = num - i - 1;
//		while (blank--)
//		{
//			printf(" ");
//		}
//		for (int j = 0; j <= i; j++)
//		{
//			printf("%2d ", arr1[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//void* my_memcpy(void* e1, void* e2, int num)
//{
//	/*for (int i = 0; i < num; i++)
//	{
//		*(char*)e1 = *(char*)e2;
//		e1 = (char*)e1 + 1;
//		e2 = (char*)e2 + 1;
//	}*/
//	while (num--)
//	{
//		*(char*)e1 = *(char*)e2;
//		e1 = (char*)e1 + 1;
//		e2 = (char*)e2 + 1;
//	}
//}
//
//int main()
//{
//	//模拟实现memcpy函数
//	//char arr1[] = "abcdefd";  //char类型
//	//char arr2[20] = { 0 };
//
//	int arr1[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	int arr2[10] = { 0 };     //整型
//
//	/*float arr1[] = { 1.2f, 1.8f, 5.7f, 8.6f, 9.4f, 8.4f };
//	float arr2[10] = { 0.0 };*/
//	my_memcpy(arr2, arr1, 9);
//	//memcpy(arr2, arr1, 4);
//	//printf("%s\n", arr2);
//	for (int i = 0; i < 3; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}

//void* my_memmove(void* e1, void* e2, size_t num)
//{
//	if ((char*)e1 - (char*)e2 <= 0)
//	{
//		while (num--)
//		{
//			*(char*)e1 = *(char*)e2;
//			e1 = *(char*)e1 + 1;
//			e2 = *(char*)e2 + 1;
//		}
//	}
//	else
//	{
//		while (num--)
//		{
//			*((char*)e1 + num) = *((char*)e2 + num);
//
//		}
//	}
//}
//
//int main()
//{
//	/*int arr1[] = { 1,2,3,4,5,6,7,8,9 };
//	int arr2[20] = { 0 };*/
//	char arr1[] = "abcdefghr";
//	//memmove(arr2, arr1, 9);
//	//my_memmove(arr2, arr1, 9);
//	my_memmove(arr1 + 3, arr1, 3);
//	for (int i = 0; i < 9; i++)
//	{
//		printf("%c ", arr1[i]);
//	}
//	return 0;
//}