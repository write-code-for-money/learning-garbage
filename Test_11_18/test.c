#define _CRT_SECURE_NO_WARNINGS 1

#include "Heap.h"

void test()
{
	Heap heap;
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	//int arr[] = { 1,12,3,41,5,6,71,8,92,10 };

	HeapCreate(&heap, arr, sizeof(arr)/sizeof(arr[0]));
	HeapPrint(&heap);
	HeapPush(&heap, 11);
	HeapPrint(&heap);
	HeapPop(&heap);
	
	HeapPrint(&heap);
	//printf("%d\n", HeapTop(&heap));
	//HeapDestory(&heap);
}

void TestTopk()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int arr1[] = { 1,12,3,41,5,6,71,8,92,10 };
	PrintTopK(arr1, sizeof(arr1)/sizeof(arr1[0]), 5);
}
int main()
{
	TestTopk();
	return 0;
}