#define _CRT_SECURE_NO_WARNINGS 1
#include "Heap.h"


// 堆的构建
void HeapCreate(Heap* hp, HPDataType* a, int n)
{
	assert(hp);
	hp->_a = (HPDataType*)malloc(sizeof(HPDataType) * n);
	memmove(hp->_a, a, sizeof(HPDataType) * n);
	hp->_capacity = hp->_size = n;
	int sz = n;
	int h = 1;
	while ((int)pow(2,h-1) < n)
	{
		for (int i = 0; i < sz / 2; i++)
		{
			int ret = i * 2 + 1;
			if (ret + 1 < n && hp->_a[ret + 1] > hp->_a[ret])
			{
				ret += 1;
			}
			if (hp->_a[ret] > hp->_a[i])
			{
				check(hp, ret, i);
			}
		}
		h++;
	}
}

void HeapPrint(Heap* hp)
{
	assert(hp);
	assert(hp->_a);
	for (int i = 0; i < hp->_size; i++)
	{
		printf("%d ", hp->_a[i]);
	}
	printf("\n");
}

// 堆的销毁
void HeapDestory(Heap* hp)
{
	assert(hp);
	free(hp->_a);
	hp->_a = NULL;
	hp->_capacity = hp->_size = 0;
}

// 堆的插入
void HeapPush(Heap* hp, HPDataType x)
{
	assert(hp);
	if (hp->_capacity >= hp->_size)
	{
		Heap* new = (Heap*)realloc(hp->_a, sizeof(Heap) * hp->_capacity * 2);
		if (new != NULL)
		{
			hp->_a = new;
			hp->_capacity *= 2;
		}
	}
	hp->_a[hp->_size++] = x;
	HeapChange(hp);
}

void HeapChange(Heap* hp)
{
	assert(hp->_a);
	int son = hp->_size - 1;
	int dad = (hp->_size - 2) / 2;
	while (son > 0)
	{
		if (hp->_a[dad] < hp->_a[son])
		{
			check(hp, dad, son);
			son = dad;
			dad = (son - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void check(Heap* hp, int x, int y)
{
	HPDataType tmp = hp->_a[x];
	hp->_a[x] = hp->_a[y];
	hp->_a[y] = tmp;
}

// 堆的删除
void HeapPop(Heap* hp)
{
	assert(hp);
	assert(hp->_a);
	if (hp->_size <= 0)
	{
		printf("Nothing to pop\n");
		return;
	}
	//1.头尾交换
	check(hp, 0, hp->_size - 1);
	hp->_size--;
	//2.向下调整
	int dad = 0;
	int son = dad * 2 + 1;
	while (son < hp->_size)
	{
		if (son + 1 < hp->_size && hp->_a[son + 1] > hp->_a[son])
		{
			son += 1;
		}
		if (hp->_a[dad] < hp->_a[son])
		{
			check(hp, dad, son);
			dad = son;
			son = dad * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

// 取堆顶的数据
HPDataType HeapTop(Heap* hp)
{
	assert(hp);
	assert(hp->_a);
	return hp->_a[0];
}

// 堆的数据个数
int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->_size;
}

// 堆的判空
int HeapEmpty(Heap* hp)
{
	assert(hp);
	return hp->_size > 0 ? 1 : 0;
}

void PrintTopK(int* a, int n, int k)
{
	Heap new_heap;
	HeapCreate(&new_heap, a, n);
	for (int i = 0; i < k; i++)
	{
		HPDataType ret = HeapTop(&new_heap);
		HeapPop(&new_heap);
		printf("%d ", ret);
	}
}
