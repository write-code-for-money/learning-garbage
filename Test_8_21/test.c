#define _CRT_SECURE_NO_WARNINGS 1

//int dominantIndex(int* nums, int numsSize) {
//    int i = 0;
//    int j = 0;
//    // if(numsSize == 1)
//    // {
//    //     return 0;
//    // }
//    for (i = 0; i < numsSize; i++)
//    {
//        for (j = 0; j < numsSize; j++)
//        {
//            if (i != j && nums[i] < 2 * nums[j])
//            {
//                break;
//            }
//        }
//        if (j == numsSize)
//        {
//            return i;
//        }
//    }
//    return -1;
//}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* intersection(int* nums1, int nums1Size, int* nums2, int nums2Size, int* returnSize) {
    int max = nums1Size > nums2Size ? nums1Size : nums2Size;
    static int arr[1000];
    int sz = 0;
    int x = 0;
    int i = 0;
    int j = 0;
    for (i = 0; i < nums1Size; i++)
    {
        for (j = 0; j < nums2Size; j++)
        {
            if (nums1[i] == nums2[j])
            {
                for (x = 0; x < sz; x++)
                {
                    if (arr[x] == nums1[i])
                    {
                        break;
                    }
                }
                if (x == sz)
                {
                    arr[sz++] = nums1[i];
                }
            }
        }
    }
    *returnSize = sz;
    return arr;
}