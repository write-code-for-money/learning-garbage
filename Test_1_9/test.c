#define _CRT_SECURE_NO_WARNINGS 1


//二叉树遍历
#include <stdio.h>
#include <malloc.h>

typedef struct tree {
    char val;
    struct tree* left;
    struct tree* right;
}Tree;

Tree* treecreat(char* arr, int* sz)
{
    if (arr[*sz] == '#')
    {
        (*sz)++;
        return NULL;
    }
    Tree* root = (Tree*)malloc(sizeof(Tree));
    root->val = arr[(*sz)++];
    root->left = treecreat(arr, sz);
    root->right = treecreat(arr, sz);
    return root;
}
void preorder(Tree* root)
{
    if (root == NULL)
    {
        return;
    }
    preorder(root->left);
    printf("%c ", root->val);
    preorder(root->right);
}

int main() {
    char arr[101];
    scanf("%s", arr);
    int sz = 0;
    Tree* root = treecreat(arr, &sz);
    preorder(root);
    return 0;
}




//单值二叉树
bool isUnivalTree(struct TreeNode* root) {
    return root == NULL || isUnivalTree(root->left) && isUnivalTree(root->right) && (root->left == NULL || root->left->val == root->val) && (root->right == NULL || root->right->val == root->val);
}

//二叉树最大深度
int maxDepth(struct TreeNode* root) {
    if (root == NULL)
    {
        return 0;
    }
    int left = maxDepth(root->left);
    int right = maxDepth(root->right);
    return left > right ? left + 1 : right + 1;
}

//相同的树

bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
    return (p == NULL && q == NULL) || q != NULL && p != NULL && isSameTree(p->left, q->left) && isSameTree(p->right, q->right) && q->val == p->val;
}

//二叉树的前序遍历
void bprive(struct TreeNode* root, int* returnSize, int* arr)
{
    if (root == NULL)
    {
        return;
    }
    arr[(*returnSize)++] = root->val;
    bprive(root->left, returnSize, arr);
    bprive(root->right, returnSize, arr);
}
int* preorderTraversal(struct TreeNode* root, int* returnSize) {
    int* arr = (int*)malloc(sizeof(int) * 100);
    *returnSize = 0;
    bprive(root, returnSize, arr);
    return arr;
}

//反转二叉树
struct TreeNode* invertTree(struct TreeNode* root) {
    if (root == NULL) {
        return NULL;
    }
    struct TreeNode* left = invertTree(root->left);
    struct TreeNode* right = invertTree(root->right);
    root->left = right;
    root->right = left;
    return root;
}

//对称二叉树
bool ischeck(struct TreeNode* left, struct TreeNode* right)
{
    if (left == NULL && right != NULL || left != NULL && right == NULL)
    {
        return false;
    }
    return left == NULL && right == NULL || left->val == right->val && ischeck(left->left, right->right) && ischeck(left->right, right->left);
}

bool isSymmetric(struct TreeNode* root) {
    return ischeck(root->left, root->right) || root == NULL;
}

//另一棵树的子树
bool issame(struct TreeNode* root, struct TreeNode* subRoot)
{
    if (root == NULL && subRoot == NULL)
    {
        return true;
    }
    if (root != NULL && subRoot == NULL || root == NULL && subRoot != NULL)
    {
        return false;
    }
    return root->val == subRoot->val && issame(root->left, subRoot->left) && issame(root->right, subRoot->right);
}

bool isSubtree(struct TreeNode* root, struct TreeNode* subRoot) {
    if (subRoot == NULL)
    {
        return true;
    }
    if (root == NULL && subRoot != NULL)
    {
        return false;
    }
    return issame(root, subRoot) || isSubtree(root->left, subRoot) || isSubtree(root->right, subRoot);
}
