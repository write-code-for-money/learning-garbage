#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int Hanoi(int n)
//{
//	if (n == 1)
//	{
//		return 1;
//	}
//	else
//	{
//		return 2 * Hanoi(n - 1) + 1;
//	}
//}
//
//int main()
//{
//	//汉诺塔问题-将n个圆盘从A柱上移动到C柱上需要多少次
//	int n = 0;
//	scanf("%d", &n);
//	int sum = Hanoi(n);
//	printf("%d\n", sum);
//	return 0;
//}

void Hanoi(int n, char A, char B, char C)
{
	if (n == 1)
	{
		printf("%c->%c\n", A, C);
	}
	else
	{
		Hanoi(n - 1, A, C, B);
		printf("%c->%c\n", A, C);
		Hanoi(n - 1, B, A, C);
	}
	
}

int main()
{
	int n = 0;
	scanf("%d", &n);
	Hanoi(n, 'A', 'B', 'C');
	return 0;
}