#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	int i = 0;
//	int j = 0;
//	int num = 0;
//	scanf("%d", &num);
//	for (i = 1; i < num; i++)
//	{
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%-2d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//void change(int* a, int* b)
//{
//	*a = *a + *b;
//	*b = *a - *b;
//	*a = *a - *b;
//}

//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	change(&a, &b);
//	printf("%d %d\n", a, b);
//	return 0;
//}

//void change(int a, int b)
//{
//	a = a + b;
//	b = a - b;
//	a = a - b;
//	printf("%d %d\n", a, b);
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	change(a, b);
//	//printf("%d %d\n", a, b);
//	return 0;
//}

//void Fu_l(int year)
//{
//	if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
//	{
//		printf("%d是闰年\n", year);
//	}
//	else
//	{
//		printf("%d不是闰年\n", year);
//	}
//}
//
//int main()
//{
//	int year = 0;
//	scanf("%d", &year);
//	Fu_l(year);
//	return 0;
//}

int Think(int x)
{
	int i = 0;
	for (i = 2; i < x; i++)
	{
		if (x % i == 0)
		{
			return 0;
		}
	}
	return 1;
}

int main()
{
	int i = 0;
	int j = 0;
	for (i = 100; i <= 200; i++)
	{
		j = Think(i);
		if (j == 1)
		{
			printf("%d ", i);
		}
	}
	return 0;
}