#define _CRT_SECURE_NO_WARNINGS 1

#include "sort.h"
#include "Queue.h"

void Print(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void Swape(int* a, int* b)
{
	int t = *a;
	*a = *b;
	*b = t;
}

//直接插入排序
void InsertSort(int* a, int n)
{
	for (int i = 1; i < n; i++)
	{
		if (a[i] < a[i - 1])
		{
			int tmp = a[i];
			int j = i - 1;
			while (a[j] > tmp && j >= 0)
			{
				a[j + 1] = a[j];
				j--;
			}
			a[j + 1] = tmp;
		}
	}


	/*int fast = 1;
	int slow = 0;
	int check = fast;
	while (fast < n)
	{
		if (a[fast] >= a[slow])
		{
			fast++;
			slow++;
		}
		else {
			check = fast;
			while (slow >= 0)
			{
				if (a[check] < a[slow])
				{
					Swape(&a[slow], &a[check]);
					slow--;
					check--;
				}
				else
				{
					break;
				}
			}
			slow = fast;
			fast++;
		}
	}*/
}

//希尔排序
void ShellSort(int* a, int n)
{
	int gap = n / 2;
	while (gap > 0)
	{
		for (int i = gap; i < n; i++)
		{
			if (a[i - gap] > a[i])
			{
				int tmp = a[i];
				int j = i - gap;
				while (j >= 0 && a[j] > tmp)
				{
					a[j + gap] = a[j];
					j -= gap;
				}
				a[j + gap] = tmp;
			}
		}
		gap /= 2;
	}
	/*for (; gap > 0; gap = gap / 2)
	{
		int slow = 0;
		int fast = slow + gap;
		int check = fast;
		while (fast < n)
		{
			if (a[fast] >= a[slow])
			{
				fast += gap;
				slow += gap;
			}
			else {
				check = fast;
				while (slow >= 0)
				{
					if (a[check] < a[slow])
					{
						Swape(&a[slow], &a[check]);
						slow -= gap;
						check -= gap;
					}
					else
					{
						break;
					}
				}
				slow = fast;
				fast += gap;
			}
		}
	}*/
}

//选择排序
void SelectSort(int* a, int n)
{
	int fast = 0;
	int end = n - 1;
	while (fast < end)
	{
		int max = fast, min = fast;
		for (int i = fast; i <= end; i++)
		{
			if (a[i] > a[max])
			{
				max = i;
			}
			if (a[i] < a[min])
			{
				min = i;
			}
		}
		if (fast == max && end == min)
		{
			Swape(&a[fast], &a[min]);
		}
		else if (fast == max && end != min)
		{
			Swape(&a[end], &a[max]);
			Swape(&a[fast], &a[min]);
		}
		else
		{
			Swape(&a[fast], &a[min]);
			Swape(&a[end], &a[max]);
		}
		fast++;
		end--;
	}
}

//堆排序
void AdjustDwon(int* a, int n, int root)
{
	for (int i = root; i >= 0; i--)
	{
		int left = i * 2 + 1;
		int right = i * 2 + 2;
		if (left < n && a[left] > a[i])
		{
			Swape(&a[left], &a[i]);
		}
		if (right < n && a[right] > a[i])
		{
			Swape(&a[right], &a[i]);
		}
	}
}

void HeapSort(int* a, int n)
{
	for (; n > 1; n--)
	{
		int root = n / 2 - 1;
		AdjustDwon(a, n, root);
		Swape(&a[0], &a[n - 1]);
	}
}

void BubbleSort(int* a, int n)
{
	while (n > 1)
	{
		for (int i = 0, j = 1; j < n; i++, j++)
		{
			if (a[i] > a[j])
			{
				Swape(&a[i], &a[j]);
			}
		}
		n--;
	}
}

//快排优化-三数取中
int Getmid(int* a, int left, int right)
{
	int mid = (right - left) / 2;
	if (a[left] < a[right])
	{
		if (a[mid] < a[left])
		{
			return left;
		}
		else if (a[mid] > a[right])
		{
			return right;
		}
		else
		{
			return mid;
		}
	}
	else
	{
		if (a[mid] > a[right])
		{
			if (a[left] > a[mid])
			{
				return mid;
			}
			else
			{
				return left;
			}
		}
		else
		{
			return right;
		}
	}
}
//快速排序
int PartSort1(int* a, int left, int right)
{
	int key = Getmid(a, left, right);
	while (left < right)
	{
		while (left < right && a[right] > a[key])
		{
			right--;
		}
		while (left < right && a[left] < a[key])
		{
			left++;
		}
		Swape(&a[left], &a[right]);
	}
	Swape(&a[left], &a[key]);
	return key;
}
//hoare版本
void QuickSort(int* a, int left, int right)
{
	if (left >= right)
	{
		return;
	}
	//递归到小区间直接插入
	if (right - left < 15)
	{
		InsertSort(a, right - left + 1);
	}
	else
	{
		int keyi = PartSort3(a, left, right);
		QuickSort(a, left, keyi - 1);
		QuickSort(a, keyi + 1, right);
	}
}

//挖坑法
int PartSort2(int* a, int left, int right)
{
	int hole = a[left];
	while (left < right)
	{
		while (left < right && a[right] > a[hole])
		{
			right--;
		}
		a[left] = a[right];
		while (left < right && a[left] < a[hole])
		{
			left++;
		}
		a[right] = a[left];
	}
	a[left] = hole;
	return left;
}

//前后指针版本
int PartSort3(int* a, int left, int right)
{
	int key = left;
	int prev = left;
	int cur = left + 1;
	while (cur <= right)
	{
		while (cur < right && a[cur] > a[key])
		{
			cur++;
		}
		if (cur < right && ++prev != cur)
		{
			Swape(&a[prev], &a[cur]);
		}
		cur++;
	}
	Swape(&a[key], &a[prev]);
	return key;
}

void StackInit(Stack* st)//初始化栈
{
	st->a = (Stacktype*)malloc(sizeof(Stacktype) * 10);
	st->top = 0;
	st->capacity = 10;
}

void StackPush(Stack* st, int data)//入栈
{
	if (st->top >= st->capacity)
	{
		st->a = (Stacktype*)realloc(st->a, sizeof(Stacktype) * st->capacity * 2);
		st->capacity *= 2;
	}
	st->a[st->top++] = data;
}

int StackTop(Stack* st)//获取栈顶
{
	return st->a[st->top - 1];
}

void StackPop(Stack* st)//出栈
{
	st->top--;
}

int StackEmpty(Stack* st)//判空
{
	return st->top == 0 ? 0 : 1;
}

void StackDestroy(Stack* st)//销毁栈
{
	free(st->a);
	st->a = NULL;
	st->capacity = 0;
	st->top = 0;
}


//快速排序非递归
void QuickSortNonR(int* a, int left, int right)
{
	Stack st;
	StackInit(&st);
	StackPush(&st, left);
	StackPush(&st, right);
	while (StackEmpty(&st) != 0)
	{
		right = StackTop(&st);
		StackPop(&st);
		left = StackTop(&st);
		StackPop(&st);

		if (right <= left)
			continue;
		int div = PartSort1(a, left, right);
		// 以基准值为分割点，形成左右两部分：[left, div) 和 [div+1, right)
		StackPush(&st, div + 1);
		StackPush(&st, right);

		StackPush(&st, left);
		StackPush(&st, div - 1);
	}
	StackDestroy(&st);
}

void _MergeSort(int* a, int prev, int cur, int* tmp)
{
	if (cur <= prev)
	{
		return;
	}
	int mid = (prev + cur) / 2;
	_MergeSort(a, prev, mid, tmp);
	_MergeSort(a, mid + 1, cur, tmp);

	int i = prev;
	int begin1 = prev, end1 = mid;
	int begin2 = mid + 1, end2 = cur;

	while (begin1 <= end1 && begin2 <= end2)
	{
		if (a[begin1] > a[begin2])
		{
			tmp[i++] = a[begin2++];
		}
		else
		{
			tmp[i++] = a[begin1++];
		}
	}
	while (begin1 <= end1)
	{
		tmp[i++] = a[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = a[begin2++];
	}
	memcpy(a + prev, tmp + prev, sizeof(int) * (cur - prev + 1));
}

// 归并排序递归实现
void MergeSort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	_MergeSort(a, 0, n - 1, tmp);
}

// 归并排序非递归实现
void MergeSortNonR(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	int rengeN = 1;
	while (rengeN < n)
	{
		for (int i = 0; i < n; i += 2 * rengeN)
		{
			int begin1 = i, end1 = i + rengeN - 1;
			int begin2 = i + rengeN, end2 = i + 2 * rengeN - 1;
			int j = i;

			if (end1 >= n)
			{
				break;
			}
			if (begin2 >= n)
			{
				break;
			}
			if (end2 >= n)
			{
				end2 = n - 1;
			}

			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] > a[begin2])
				{
					tmp[j++] = a[begin2++];
				}
				else
				{
					tmp[j++] = a[begin1++];
				}
			}
			while (begin1 <= end1)
			{
				tmp[j++] = a[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[j++] = a[begin2++];
			}
			memcpy(a + i, tmp + i, sizeof(int) * (end2 - i + 1));
		}
		rengeN *= 2;
	}
	free(tmp);
	tmp = NULL;
}

int Getkey(int data, int t)
{
	int ret = 0;
	while (t > 0)
	{
		ret = data % 10;
		data /= 10;
		t--;
	}
	return ret;
}
// 计数排序
void CountSort(int* a, int n)
{
	Queue* arr[10];
	for (int i = 0; i < n; i++)
	{
		arr[i] = (Queue*)malloc(sizeof(Queue));
		QueueInit(arr[i]);
	}
	for (int i = 1; i <= Max; i++)
	{
		for (int j = 0; j < n; j++)
		{
			int key = Getkey(a[j], i);
			QueuePush(arr[key], a[j]);
		}
		int tmp = 0;
		for (int j = 0; j < n; j++)
		{
			while (!QueueEmpty(arr[j]))
			{
				int ret = QueueFront(arr[j]);
				QueuePop(arr[j]);
				a[tmp++] = ret;
			}
		}
	}
}