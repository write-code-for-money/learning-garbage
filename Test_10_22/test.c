#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//struct book
//{
//    int i;
//    int x;
//}str1, str2;
//
//int main()
//{
//    str1.i = 1;
//    str1.x = 3;
//    str2 = str1;
//    printf("%d %d\n", str2.i, str2.x);
//    return 0;
//}

//int main()
//{
//    FILE* pFile;
//    //打开文件
//    pFile = fopen("test.txt", "w");
//    //指针判断
//    if (pFile == NULL)
//    {
//        printf("打开失败！\n");
//        exit(-1);
//    }
//    //文件操作
//    
//
//
//
//    //关闭文件
//    fclose(pFile);
//    return 0;
//}

//int missingNumber(int* nums, int numsSize) {
//    int sum = 0;
//    int i = 0;
//    for (i = 0; i < numsSize; i++)
//    {
//        sum ^= nums[i];
//    }
//    for (i = 0; i < numsSize + 1; i++)
//    {
//        sum ^= i;
//    }
//    return sum;
//}

//void rotate(int* nums, int numsSize, int k) {
//    int i = 0;
//    int j = 0;
//    while (k > numsSize)
//    {
//        k = k - numsSize;
//    }
//    for (i = 0, j = numsSize - k - 1; i < j; i++, j--)
//    {
//        nums[i] ^= nums[j];
//        nums[j] ^= nums[i];
//        nums[i] ^= nums[j];
//    }
//    for (i = numsSize - k, j = numsSize - 1; i < j; i++, j--)
//    {
//        nums[i] ^= nums[j];
//        nums[j] ^= nums[i];
//        nums[i] ^= nums[j];
//    }
//    for (i = 0, j = numsSize - 1; i < j; i++, j--)
//    {
//        nums[i] ^= nums[j];
//        nums[j] ^= nums[i];
//        nums[i] ^= nums[j];
//    }
//}

//#include <stdio.h>
//int main()
//{
//	char arr[] = "abcdefg";
//	FILE* p = fopen("text.txt", "w");
//	if (p == NULL)
//	{
//		printf("打开失败！\n");
//		exit(-1);
//	}
//	for (int i = 0; i < 26; i++)
//	{
//		fputc('a' + i, p);
//	}
//	fclose(p);
//	p = NULL;
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	char arr[] = "abcdefg";
//	FILE* p = fopen("text.txt", "r");
//	if (p == NULL)
//	{
//		printf("打开失败！\n");
//		exit(-1);
//	}
//	for (int i = 0; i < 26; i++)
//	{
//		printf("%c ", fgetc(p));
//	}
//	printf("\n");
//	fclose(p);
//	p = NULL;
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	FILE* p = fopen("text.txt", "w");
//	if (p == NULL)
//	{
//		printf("打开失败！\n");
//		exit(-1);
//	}
//	fputs("hello\n", p);
//	fputs("world", p);
//	fclose(p);
//	p = NULL;
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	char arr[] = "################";
//	FILE* p = fopen("text.txt", "r");
//	if (p == NULL)
//	{
//		printf("打开失败！\n");
//		exit(-1);
//	}
//	fgets(arr, 8, p);
//
//	fclose(p);
//	p = NULL;
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	FILE* p = fopen("text.txt", "w");
//	if (p == NULL)
//	{
//		printf("打开失败！\n");
//		exit(-1);
//	}
//	int i = 5;
//	char arr[] = "abcdef";
//	fprintf(p, "%d %s", i, arr);
//
//	fclose(p);
//	p = NULL;
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	FILE* p = fopen("text.txt", "r");
//	if (p == NULL)
//	{
//		printf("打开失败！\n");
//		exit(-1);
//	}
//	int x = 0;
//	char arr[] = "#########";
//	fscanf(p, "%d %s", &x, arr);
//
//	fclose(p);
//	p = NULL;
//	return 0;
//}

//#include <stdio.h>
//
//struct people
//{
//	char name[20];
//	int age;
//};
//int main()
//{
//	FILE* p = fopen("text.txt", "wb");
//	if (p == NULL)
//	{
//		printf("打开失败！\n");
//		exit(-1);
//	}
//	struct people peop = { "wang", 15 };
//	fwrite(&peop, sizeof(peop), 1, p);
//
//	fclose(p);
//	p = NULL;
//	return 0;
//}

#include <stdio.h>

struct people
{
	char name[20];
	int age;
};
int main()
{
	FILE* p = fopen("text.txt", "rb");
	if (p == NULL)
	{
		printf("打开失败！\n");
		exit(-1);
	}
	struct people peop;
	fread(&peop, sizeof(peop), 1, p);

	fclose(p);
	p = NULL;
	return 0;
}