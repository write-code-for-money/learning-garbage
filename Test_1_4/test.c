#define _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h"
void test()
{
	Stack ps;
	// 初始化栈
	StackInit(&ps);
	//入栈
	StackPush(&ps, 1);
	StackPush(&ps, 2);
	StackPush(&ps, 3);
	StackPush(&ps, 4);
	StackPush(&ps, 5);
	StackPush(&ps, 6);
	Print(&ps);
	printf("%d\n", StackSize(&ps));
	//出栈
	StackPop(&ps);
	StackPop(&ps);
	StackPop(&ps);
	Print(&ps);
	printf("%d\n", StackSize(&ps));

	printf("%d\n", StackTop(&ps));

	StackPop(&ps);
	Print(&ps);
	printf("%d\n", StackSize(&ps));

	//获取栈顶元素
	printf("%d\n", StackTop(&ps));
	StackDestroy(&ps);
}

int main()
{
	Stack ps;
	// 初始化栈
	StackInit(&ps);
	//入栈
	StackPush(&ps, 1);
	StackPush(&ps, 2);
	StackPush(&ps, 3);
	StackPush(&ps, 4);
	StackPush(&ps, 5);
	StackPush(&ps, 6);
	//出栈
	StackPop(&ps);
	StackPop(&ps);
	StackPop(&ps);
	//获取栈顶元素
	STDataType ret = StackTop(&ps);
	// 获取栈中有效元素个数
	int sz = StackSize(&ps);
	// 检测栈是否为空，如果为空返回非零结果，如果不为空返回0 
	int t = StackEmpty(&ps);
	// 销毁栈
	StackDestroy(&ps);
	return 0;
}