#define _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h"
// 初始化栈
void StackInit(Stack* ps)
{
	assert(ps);
	ps->_a = (STDataType*)malloc(sizeof(STDataType) * 5);
	ps->_capacity = 5;
	ps->_top = 0;
}

// 入栈
void StackPush(Stack* ps, STDataType data)
{
	assert(ps);
	if (ps->_capacity == ps->_top)
	{
		//扩容
		STDataType* p = (STDataType*)realloc(ps->_a, sizeof(STDataType) * ps->_capacity * 2);
		if (p == NULL)
		{
			printf("realloc fail\n");
			return;
		}
		else {
			ps->_a = p;
			ps->_capacity *= 2;
			printf("realloc finish\n");
		}
	}
	ps->_a[ps->_top++] = data;
}

// 出栈
void StackPop(Stack* ps)
{
	assert(ps);
	if (ps->_top <= 0)
	{
		printf("nothing to pop\n");
		return;
	}
	ps->_top--;
}

// 获取栈顶元素
STDataType StackTop(Stack* ps)
{
	assert(ps);
	assert(ps->_a);
	return ps->_a[ps->_top - 1];
}

void Print(Stack* ps)
{
	assert(ps);
	assert(ps->_a);
	for (int i = 0; i < ps->_top; i++)
	{
		printf("%d ", ps->_a[i]);
	}
	printf("\n");
}

// 获取栈中有效元素个数
int StackSize(Stack* ps)
{
	assert(ps);
	return ps->_top;
}

// 检测栈是否为空，如果为空返回非零结果，如果不为空返回0 
int StackEmpty(Stack* ps)
{
	assert(ps);
	return ps->_top > 0 ? 0 : 1;
}

// 销毁栈
void StackDestroy(Stack* ps)
{
	free(ps->_a);
	ps->_a = NULL;
	ps->_capacity = 0;
	ps->_top = 0;
}