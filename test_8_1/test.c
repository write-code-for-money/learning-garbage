#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main()
{
    float money = 0;
    int month = 0;
    int day = 0;
    int val = 0;
    float total = 0;
    scanf("%f %d %d %d", &money, &month, &day, &val);
    if (val == 1)
    {
        if (month == 11 && day == 11)
        {
            total = money * 0.7 - 50.0;
            if (total >= 0)
            {
                printf("%.2f\n", total);
            }
            else
                printf("0.00\n");
        }
        if (month == 12 && day == 12)
        {
            total = money * 0.8 - 50.0;
            if (total >= 0)
            {
                printf("%.2f\n", total);
            }
            else
                printf("0.00\n");
        }
    }
    else {
        if (month == 11 && day == 11)
        {
            printf("%.2f\n", money * 0.7);
        }
        if (month == 12 && day == 12)
        {
            printf("%.2f\n", money * 0.8);
        }
    }
    return 0;
}